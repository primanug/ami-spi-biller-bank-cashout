#!/usr/bin/groovy
@Library('pipeline_library@1.8.1-release')
import groovy.json.*
import java.text.SimpleDateFormat
import java.io.File
import groovy.json.JsonSlurperClassic

// Global Variable Declartion
def GLOBAL_VARS = ""
def APP_NAME = ""
def APP_VERSION = ""
def GIT_HASH = ""
def GIT_TAG = ""
def GIT_AUTHOR = ""
def GIT_HASH_OPENSHIFT_CONFIGURATION = ""
def GIT_HASH_ECS_CONFIGURATION = ""
def GIT_HASH_TESSERACT_CONFIGURATION = ""
def LIST_ENV = ["dev", "qa", "performance", "staging", "production"]
def RERUN_CONDITION_ACTION = ""
def APP_URL_TYPE_SERVICE = ""
def APPLICATION_MOUNTEBANK_EXISTING = "none"
def conditionAction = ""
def images = []
def stageCurrentDEV = ""
def stageCurrentQA = ""
def stageCurrentPerformance = ""
def sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
// sdf.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
def startTimePipeline = ""
def endTimePipeline = ""

def SKIP_DEV_ENV = []
def SKIP_QA_ENV = []
def SKIP_PERFORMANCE_ENV = []

def stageDevStatus = ""
def stageDevPipelineResult = ""

def stageQaStatus = ""
def stageQaPipelineResult = ""

def stagePerformanceStatus = ""
def stagePerformancePipelineResult = ""

def git_hash_configuration = ""
def countryCode = ""
def imageApplication = ""
def imageApplicationMountebank = ""
def openshiftVersion = ""
def namespace_cicd = ""
def namespace_dev = ""
def namespace_qa = ""
def namespace_performance = ""
def namespace_cicd_common = "common-cicd-platform"
def FORCE_DEPLOY_NETWORK_POLICY_FIRST_BUILD = "false"
def FORCE_DEPLOY_AUTOSCALING_FIRST_BUILD = "false"
def FORCE_DEPLOY_ROUTE_FIRST_BUILD = "false"
def RESOURCE_LIMIT_MEMORY_BUILDER = ""
def RESOURCE_LIMIT_CPU_BUILDER = ""
def RESOURCE_LIMIT_MEMORY_TESTER = ""
def RESOURCE_LIMIT_CPU_TESTER = ""
def TESTER_IMAGE_NAME = ""
def convertBuildNumber = env.BUILD_NUMBER.toInteger()
def increaseConvertBuildNumber = convertBuildNumber+1
def jobName = env.JOB_NAME
def userOtherJenkin = ""
def passOtherJenkin = ""

properties([
  parameters([
    booleanParam(
      name: 'Skip_Build_Image', 
      defaultValue: false, 
      description: 'If selected, pipeline will not rebuild app image but will pull from GCR and deploy to Dev instead'),
    string(
      name: 'Image_Version_from_GCR', 
      defaultValue: '',
      description: 'Ignore this if you do not select "Skip_Build_Image".\nIf selected, please specify the version of your app (docker image tag) that you would like to pull from GCR ex: 1.0.1-234\n\n\n'),
    string(
      name: 'Tester_Image_Name', 
      defaultValue: 'default',
      description: 'Ex. "tester:v1.0.22" \nIf value is "default" will use default tester image of that pipeline version.'),
    string(
      name: 'BuildOnBranch', 
      defaultValue: 'master', 
      description: 'Branch of git repo is you want to pull'),
    string(
      name: 'ResourceLimitMemoryBuilder', 
      defaultValue: "2048Mi",
      description: 'Maximum memory limit allocation for maven builder container'),
    string(
      name: 'ResourceLimitCPUBuilder', 
      defaultValue: "500m",
      description: 'Maximum cpu limit allocation for maven builder container'),
    string(
      name: 'ResourceLimitMemoryTester', 
      defaultValue: "1024Mi",
      description: 'Maximum memory limit allocation for tester builder container'),
    string(
      name: 'ResourceLimitCPUTester', 
      defaultValue: "100m",
      description: 'Maximum cpu limit allocation for tester builder container'),
    choice(
      name: 'RerunCondition', 
      choices: 'ignore\ndev\nqa\nsystem_integration_test\nperformance', 
      description: 'Ignore : Deploy application\nDev : Rerun only run-integration-test on dev environment\nQa : Rerun only run-integration-test on qa environment\nSystemIntegrationTest : Rerun only run-system-integration-test on performance environment\nPerformance : Rerun only run-performance-test on performance environment'),
    booleanParam(
      name: 'BuildArtifactsOnlyAndSkipOpenshiftDeploy', 
      defaultValue: false, 
      description: 'Check : Skip all stages for compress artifact only\nUncheck : Original Pipeline'),
    booleanParam(
      name: 'SkipRunPerformanceTest', 
      defaultValue: false, 
      description: 'Check : Skip run performance stages on performance environment\nUncheck : Original Pipeline'),
    booleanParam(
      name: 'ForceDeployNetworkPolicy', 
      defaultValue: false, 
      description: 'Check : If need to force apply networkpolicy changed for this build version'),
    booleanParam(
      name: 'ForceDeployRoute', 
      defaultValue: false,
      description: 'Check : If need to force apply route changed for this build version\n\n'),
  ]), 
  [
    $class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '10']
  ]
])

try {
  BUILD_ON_BRANCH = BuildOnBranch
  RERUN_CONDITION_ACTION = RerunCondition
  BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = BuildArtifactsOnlyAndSkipOpenshiftDeploy
  SKIP_RUN_PERFORMANCE_TEST = SkipRunPerformanceTest
  FORCE_DEPLOY_NETWORK_POLICY = ForceDeployNetworkPolicy
  FORCE_DEPLOY_ROUTE = ForceDeployRoute
  RESOURCE_LIMIT_MEMORY_BUILDER = ResourceLimitMemoryBuilder
  RESOURCE_LIMIT_CPU_BUILDER = ResourceLimitCPUBuilder
  RESOURCE_LIMIT_MEMORY_TESTER = ResourceLimitMemoryTester
  RESOURCE_LIMIT_CPU_TESTER = ResourceLimitCPUTester
  TESTER_IMAGE_NAME = Tester_Image_Name
  SKIP_BUILD_IMAGE = Skip_Build_Image
  IMAGE_VERSION_GCR = Image_Version_from_GCR
} catch(Throwable e) {
  BUILD_ON_BRANCH = "master"
  RERUN_CONDITION_ACTION = "ignore"
  BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = "false"
  SKIP_RUN_PERFORMANCE_TEST = "false"
  FORCE_DEPLOY_NETWORK_POLICY = "true"
  FORCE_DEPLOY_NETWORK_POLICY_FIRST_BUILD = "true"
  FORCE_DEPLOY_ROUTE = "true"
  FORCE_DEPLOY_ROUTE_FIRST_BUILD = "true"
  RESOURCE_LIMIT_MEMORY_BUILDER = "2048Mi"
  RESOURCE_LIMIT_CPU_BUILDER = "2048m"
  RESOURCE_LIMIT_MEMORY_TESTER = "1024Mi"
  RESOURCE_LIMIT_CPU_TESTER = "1024m"
  TESTER_IMAGE_NAME = "default"
  SKIP_BUILD_IMAGE = "false"
  IMAGE_VERSION_GCR = ""
}

if ( TESTER_IMAGE_NAME == "" ) {
  error "Please fill in tester image name."
}

if ( SKIP_BUILD_IMAGE == "true" && IMAGE_VERSION_GCR == "") {
  error "Empty Image version. If you choose SKIP_BUILD_IMAGE is true. You need to enter image version to deploy."
}  

mavenNode( 
  testerImageName: TESTER_IMAGE_NAME,
  resourceLimitMemoryMaven: RESOURCE_LIMIT_MEMORY_BUILDER, 
  resourceLimitCpuMaven: RESOURCE_LIMIT_CPU_BUILDER, 
  resourceLimitMemoryTester: RESOURCE_LIMIT_MEMORY_TESTER, 
  resourceLimitCpuTester: RESOURCE_LIMIT_CPU_TESTER ) {

  def directory_workspace = pwd()

  git branch: BUILD_ON_BRANCH, credentialsId: 'bitbucket-credential', url: 'https://bitbucket.org/ascendcorp/ami-id-eq-spi-biller.git'

  script {
      GLOBAL_VARS = readProperties  file:'pipeline/variables.properties'
      GIT_HASH = sh script: "cd ${directory_workspace} && git rev-parse --verify HEAD", returnStdout: true
      GIT_HASH = "$GIT_HASH".trim()
      GIT_AUTHOR = sh script: "git log -1 --pretty=format:%an", returnStdout: true
      namespace_cicd = "${GLOBAL_VARS['APP_SCOPE']}-cicd"
      namespace_dev = "${GLOBAL_VARS['APP_SCOPE']}-${GLOBAL_VARS['APP_SVC_GROUP']}-dev"
      namespace_qa = "${GLOBAL_VARS['APP_SCOPE']}-${GLOBAL_VARS['APP_SVC_GROUP']}-qa"
      namespace_performance = "${GLOBAL_VARS['APP_SCOPE']}-${GLOBAL_VARS['APP_SVC_GROUP']}-performance"
  }

  if ( GLOBAL_VARS['AUTO_PUSH_TAG'] == "true" && SKIP_BUILD_IMAGE == "false" ) {

    echo "AUTO_PUSH_TAG : true"

    container(name: 'maven') { 
      script {
        GIT_TAG_FROM_POM = sh script: "mvn org.apache.maven.plugins:maven-help-plugin:3.1.0:evaluate -Dexpression=project.version -q -DforceStdout", returnStdout: true
        GIT_TAG_FROM_POM = "$GIT_TAG_FROM_POM".trim()
      }
    }

    script {
      GIT_TAG_FROM_LATEST_TAG = sh script: "git describe --tags `git rev-list --tags --max-count=1`", returnStdout: true
      GIT_TAG_FROM_LATEST_TAG = "$GIT_TAG_FROM_LATEST_TAG".trim()
      APP_VERSION="$GIT_TAG_FROM_POM-${env.BUILD_NUMBER}" 
    }

    if( GIT_TAG_FROM_POM != GIT_TAG_FROM_LATEST_TAG ) {
      script {
        withCredentials([usernamePassword(credentialsId: 'bitbucket-credential', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
          def giturl_push = GLOBAL_VARS['GIT_SOURCE_URL'].split("//")[1]
          sh "git tag ${GIT_TAG_FROM_POM}"
          sh "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${giturl_push} ${GIT_TAG_FROM_POM}"
        }
      }
    } else {
      echo "Ignore to push tag because tag on pom.xml and latest tag it does match" 
    }
    
    script {
      APP_VERSION="$GIT_TAG_FROM_POM-${env.BUILD_NUMBER}" 
    }

  } else {

    echo "AUTO_PUSH_TAG : false or You're skip build image."
    
    script {
      GIT_TAG = sh script: "git describe --tags `git rev-list --tags --max-count=1`", returnStdout: true
      GIT_TAG = "$GIT_TAG".trim()
      APP_VERSION="$GIT_TAG-${env.BUILD_NUMBER}"
    }

  }

  if ( SKIP_BUILD_IMAGE == "true" ) {
    //replace App Version 
    APP_VERSION = IMAGE_VERSION_GCR
  }

  echo  "importing common method"
  def common = load "pipeline/common-method/common.groovy"

  container(name: 'jnlp') {

    // GET OPENSHIFT VERSION
    openshiftVersion = acnGetVersionOpenshift()

    // GET COUNTRY CODE
    countryCode = sh script: "cat /country-code/countrycode.txt", returnStdout: true
    countryCode = countryCode.trim()

    sh "oc label pod ${HOSTNAME} appScope=openshift-cicd appName=openshift-cicd"

    // SYNC JENKINS JOB'S BUILD NUMBER (TH ONLY)
    if(countryCode == "TH") {
      common.syncBuildNumber(namespace_cicd, jobName, increaseConvertBuildNumber)
    } else {
        println "Ignore sync build number"
    }

  }

  def fileCommit = sh (
    script: 'echo "$(git diff --name-only HEAD HEAD~1)" | tr \'\n\' \' \' ',
    returnStdout: true
  ).trim()
  def listFileCommit = []
  def listFileCommitBoolean = []
  listFileCommit = fileCommit.tokenize(' ')
  for(file in listFileCommit) {
    listFileCommitBoolean.add(file.contains("pipeline/openshift-artifacts/${openshiftVersion}/mountebank"))
  }

  // Push data to trackingVersionList
  def trackingVersionList = []
  trackingVersionList.add(GIT_TAG) // 0
  trackingVersionList.add(GIT_AUTHOR) // 1
  trackingVersionList.add(GIT_HASH) // 2
  trackingVersionList.add(RERUN_CONDITION_ACTION) // 3
  trackingVersionList.add(BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY) // 4
  trackingVersionList.add(BUILD_ON_BRANCH) // 5
  trackingVersionList.add(FORCE_DEPLOY_NETWORK_POLICY) // 6
  trackingVersionList.add("false") // 7 FORCE_DEPLOY_AUTO_SCALING
  trackingVersionList.add(FORCE_DEPLOY_ROUTE) // 8
  trackingVersionList.add(FORCE_DEPLOY_NETWORK_POLICY_FIRST_BUILD) // 9
  trackingVersionList.add("false") // 10 FORCE_DEPLOY_AUTO_SCALING_FIRST_BUILD
  trackingVersionList.add(FORCE_DEPLOY_ROUTE_FIRST_BUILD) // 11
  trackingVersionList.add(GLOBAL_VARS['DATABASE_DEPLOY']) // 12

  try {

    startTimePipeline = sdf.format(new Date())

    // ## STAGE 1 : Update Configuration
    stage('Update Configuration') {

      stageCurrentDEV = "DEV_Update-Configuration-For-Dev-Environment"

      container(name: 'maven') {

        echo 'Update Configuration for \'dev\' environment was run in \'maven\'container '

        if(SKIP_BUILD_IMAGE == "false") {

          if ( RERUN_CONDITION_ACTION == "ignore" ) {
            
            SKIP_DEV_ENV.add("false")
            common.updateConfig(directory_workspace, GLOBAL_VARS, countryCode, APP_VERSION, trackingVersionList)

          } else {
            echo "SKIP Update configuration due to rerun condition is ${RERUN_CONDITION_ACTION}"
          }

        } else {
          echo "SKIP Update configuration due to SKIP_BUILD_IMAGE is ${SKIP_BUILD_IMAGE}"
        }

      }

    }

    // ## STAGE 2 : Unit test & Source Analysis & Build Jar Package
    stage('Unit test & Source Analysis & Build Jar Package') {

      stageCurrentDEV = "DEV_Unit-Test-And-Source-Analysis-and-Build-Jar-Package"

      container(name: 'maven') {

        echo 'Unit test & Source Analysis & Build Jar Package for \'dev\' environment was run in \'maven\'container '

        if(SKIP_BUILD_IMAGE == "false") {

          if ( RERUN_CONDITION_ACTION == "ignore" ) {

            try {
              
              SKIP_DEV_ENV.add("false")
              acnMavenBuildandCodeAnalysis {
                global_vars = GLOBAL_VARS
                VERSION = APP_VERSION
              } 

            } catch(e) {
              echo 'Error : Unit test & Source Analysis & Build Jar Package was failed!'
              throw e
            }

          } else {
            echo "SKIP Unit test & Source Analysis & Build Jar Package due to rerun condition is ${RERUN_CONDITION_ACTION}"
          } 

        } else {
          echo "SKIP Unit test & Source Analysis & Build Jar Package due to SKIP_BUILD_IMAGE is ${SKIP_BUILD_IMAGE}"
        }

      }

    } // End stage Source analysis

    // ## STAGE 3 : SonarQube Quality Gate
    stage('SonarQube Quality Gate') {

      stageCurrentDEV = "DEV_SonarQube-Quality-Gate"

      container(name: 'maven') {

        echo 'SonarQube Quality Gate for \'dev\' environment was run in \'maven\'container '

        if(SKIP_BUILD_IMAGE == "false") {

          if ( RERUN_CONDITION_ACTION == "ignore" && GLOBAL_VARS['SONARQUBE_ENABLE'].toInteger() == 1 ) {
            
            SKIP_DEV_ENV.add("false")
            common.sonarQube(GLOBAL_VARS)

          } else {
            echo "SKIP SonarQube quality check due to rerun condition is ${RERUN_CONDITION_ACTION}, sonarqube enable is ${GLOBAL_VARS['SONARQUBE_ENABLE']}"
          }

        } else {
          echo "SKIP SonarQube quality check due to SKIP_BUILD_IMAGE is ${SKIP_BUILD_IMAGE}"
        } 

      }

    } // End stage sonarqube quality gate

    // ## STAGE 4 : Build Images
    stage('Build APP & MB Image') {

      stageCurrentDEV = "DEV_Build-Images"

      container(name: 'maven') {

        echo 'Build Application & Mountebank Image for \'dev\' environment was run in \'maven\'container '

        if ( RERUN_CONDITION_ACTION == "ignore" && GLOBAL_VARS['DEPLOY_MOUNTBANK'].toInteger() == 1 ) {

          try {

            sh "curl http://${GLOBAL_VARS['APP_NAME']}-mountebank.${namespace_dev}.svc:2525"
            APPLICATION_MOUNTEBANK_EXISTING = "application-MB-Existing"

          } catch(e) {

            APPLICATION_MOUNTEBANK_EXISTING = "application-MB-Not-Existing"

          }

        }

        if(SKIP_BUILD_IMAGE == "false") {

          if ( RERUN_CONDITION_ACTION == "ignore" ) {
            
            SKIP_DEV_ENV.add("false")
            images = common.devBuildImages(directory_workspace, GLOBAL_VARS, countryCode, openshiftVersion, APP_VERSION, namespace_cicd, namespace_dev, APPLICATION_MOUNTEBANK_EXISTING, listFileCommitBoolean, trackingVersionList)

          } else {
            echo "SKIP Build images due to rerun condition is ${RERUN_CONDITION_ACTION}"
          }

        } else {
          echo "SKIP Build Images due to SKIP_BUILD_IMAGE is ${SKIP_BUILD_IMAGE}"
        }

      }

    } // End stage Build Images
      
    // ## STAGE 5 : Pull Image From GCR ( ** only run when skip building images ** )
    stage('Pull APP Image From GCR & Build MB') {

      stageCurrentDEV = "DEV_Pull-Image"

      container(name: 'jnlp') {

        echo 'Pull Application Image From GCR & Build Mountebank for \'dev\' environment was run in \'jnlp\'container '

        if(SKIP_BUILD_IMAGE == "false") {

          echo "SKIP Pulling image from GCR due to image was just builded in previous stage (Build APP & MB Image)"

        } else {

          def NAMESPACE_CICD = GLOBAL_VARS['APP_SCOPE'] + "-cicd"

          images = [ "app" ]

          parallel 'Pull Application Image From GCR': {

            // Pull Application Image From GCR
                
            dockerRegistry = acnGetDockerRegistryServiceHost()
            countryCodeLower = countryCode.toLowerCase()

            fileName = GLOBAL_VARS['APP_NAME'] + "-" + APP_VERSION

            if ( GLOBAL_VARS['APP_SCOPE'] == "truemoney" || GLOBAL_VARS['APP_SCOPE'] == "equator" ) {
              gcrProjectName = "tmn-" + countryCodeLower + "-ci"
              if ( GLOBAL_VARS['APP_SCOPE'] == "equator" ) {
                gcrProjectName = "tmn-th-equator-ci"
              } 
            } else if ( GLOBAL_VARS['APP_SCOPE'] == "nano" ) {
              gcrProjectName = "nano-prod"
            }

            def localBCFile = "${directory_workspace}/pipeline/openshift-artifacts/${openshiftVersion}/buildconfigs/pull-image-buildconfig.yaml"

            sh "sed -i \"s~#ENV_NAME#~Dev~g\" ${localBCFile} && \
              sed -i \"s~#NAMESPACE#~${NAMESPACE_CICD}~g\" ${localBCFile} && \
              sed -i \"s~#GCR_PROJECT_NAME#~${gcrProjectName}~g\" ${localBCFile} && \
              sed -i \"s~#APP_VERSION#~${APP_VERSION}~g\" ${localBCFile} && \
              sed -i \"s~#DOCKER_REGISTRY_SERVICE_IP#~${dockerRegistry}~g\" ${localBCFile}"

            // # GitOps : Checkout
            gitOps.checkout(directory_workspace, GLOBAL_VARS['APP_SCOPE'], "dev", GLOBAL_VARS['APP_NAME'], APP_VERSION, GLOBAL_VARS['GITOPS_REPO_NAME'])
            // # GitOps : Add artifact file 'pull-image-buildconfig.yaml'
            gitOps.addFile(directory_workspace, GLOBAL_VARS['APP_SCOPE'], "dev", GLOBAL_VARS['APP_NAME'], APP_VERSION, GLOBAL_VARS['GITOPS_REPO_NAME'], localBCFile, "BC", "pull-image-buildconfig.yaml")
            // # GitOps : Push to repository
            gitOps.push(directory_workspace, GLOBAL_VARS['APP_SCOPE'], "dev", GLOBAL_VARS['APP_NAME'], APP_VERSION, GLOBAL_VARS['GITOPS_REPO_NAME'])
            
            sh "oc apply -f ${localBCFile} -n ${NAMESPACE_CICD}"
            sh "oc start-build ${GLOBAL_VARS['APP_NAME']}-pull-images-buildconfig --follow --wait -n ${NAMESPACE_CICD}"

            // SET IMAGES VARIABLE
            imageApplication = "${dockerRegistry}:5000/${NAMESPACE_CICD}/${GLOBAL_VARS['APP_NAME']}:${APP_VERSION}"
            images.set(0, imageApplication)

          }, 'Build Mountebank Image': {

            // Build Mountebank Image
            common.buildMountBank(directory_workspace, GLOBAL_VARS, APP_VERSION, NAMESPACE_CICD, APPLICATION_MOUNTEBANK_EXISTING, listFileCommitBoolean, countryCode, openshiftVersion, BUILD_ON_BRANCH, namespace_dev)

          }

        }

      }

    }

    // ## STAGE 6 : Images vulnerability scan
    stage('Images vulnerability scan') {

      stageCurrentDEV = "DEV_Scan-Images"

      container(name: 'maven') {

        echo 'Images vulnerability scan for \'dev\' environment was run in \'maven\'container '

        if ( RERUN_CONDITION_ACTION == "ignore" ) {
            
          SKIP_DEV_ENV.add("false")
          common.devScanImages(directory_workspace, GLOBAL_VARS, images, countryCode, openshiftVersion, APP_VERSION)

        } else {
          echo "SKIP Image vulnerability scan due to rerun condition is ${RERUN_CONDITION_ACTION}"
        }

      }

    } // End stage image vulnerability scan

    // ## STAGE 7 : Images vulnerability scan
    stage('Artifact Distributed') {

      stageCurrentDEV = "DEV_Artifact-Distributed"

      container(name: 'maven') {

        echo 'Artifact Distributed for \'dev\' environment was run in \'maven\'container '

        if(SKIP_BUILD_IMAGE == "false") {

          if ( RERUN_CONDITION_ACTION == "ignore" ) {
            
            SKIP_DEV_ENV.add("false")
            common.devArtifactDist(directory_workspace, GLOBAL_VARS, countryCode, APP_VERSION, GIT_HASH, images, trackingVersionList, GIT_HASH_OPENSHIFT_CONFIGURATION, openshiftVersion)

          } else {
            echo "SKIP Artifact distributed due to rerun condition is ${RERUN_CONDITION_ACTION}" 
          }

        } else {
          echo "SKIP Artifact distributed due to skip building image"
        }

      }
      
    } // End stage Artifact Distribute

    // ## STAGE 8 : Sync Configuration Server
    stage('Sync Configuration Server') {

      stageCurrentDEV = "DEV_Sync-Configuration-Server"

      container(name: 'maven') {

        echo 'Sync Configuration Server for \'dev\' environment was run in \'maven\'container '

        if ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" ) {

          SKIP_DEV_ENV.add("false")
          common.devSyncConfig(directory_workspace, GLOBAL_VARS, APP_VERSION, countryCode, LIST_ENV)

        } else {
          echo "SKIP Sync configuration server due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}"
        }

      }

    } // End stage Sync config

    // ## STAGE 9 : Deployment Resources For Dev Environment
    stage('Deployment Resources For Dev Environment') {

      stageCurrentDEV = "DEV_Deployment-Resources-For-Dev-Environment"

      container(name: 'maven') {

        echo 'Deployment Resources for \'dev\' environment was run in \'maven\'container '

        if ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" ) {

          SKIP_DEV_ENV.add("false")
          common.devDeployResToDev(directory_workspace, GLOBAL_VARS, countryCode, openshiftVersion, APP_VERSION, namespace_cicd, LIST_ENV, namespace_dev, APPLICATION_MOUNTEBANK_EXISTING, listFileCommitBoolean, trackingVersionList)

        } else {
          echo "SKIP Deploy resource to DEV Environment due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}"
        } 

      }

    }

    // ## STAGE 10 : Verify application version has changed (DEV)
    stage('Verify application version has changed') {

      stageCurrentDEV = "DEV_Verify-version-application-has-changed"

      container(name: 'maven') {

        if ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" ) {
          
          SKIP_DEV_ENV.add("false")
          common.devVerifyVersion(APP_URL_TYPE_SERVICE, GLOBAL_VARS, namespace_dev, APP_VERSION)

        } else {
          echo "SKIP Verify application version due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}"
        } 

      }

    } // End stage verify version

    // ## STAGE 11 : Run Integration Test
    stage('Run Integration Test') {

      stageCurrentDEV = "DEV_Run-Integration-Test"

      container(name: 'tester') {

        echo 'Run integration test for \'dev\' environment by \'tester\'container '

        if ( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" ) || ( RERUN_CONDITION_ACTION == "dev" ) ) {
          
          SKIP_DEV_ENV.add("false")
          common.devRunIntegrationTest(directory_workspace, GLOBAL_VARS, RERUN_CONDITION_ACTION, APP_VERSION, APP_URL_TYPE_SERVICE, trackingVersionList)

        } else {
          echo "SKIP Run integration test due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}"
        }

      }

    }

    if(SKIP_DEV_ENV.contains("false")){
      stageDevPipelineResult = "SUCCESS"
    } else {
      stageDevPipelineResult = "Skip all stage in dev environment"
    }

  } catch(e) {

    stageDevStatus = "error"
    stageDevPipelineResult = "FAIL_${stageCurrentDEV}"

    error "Pipeline was failed on stage : ${stageCurrentDEV} "

    throw e

  } finally {

    endTimePipeline = sdf.format(new Date())

    // SEND PIPELINE RESULT TO ELASTICSEARCH (DEV)
    def sendElsAppVersion = stageDevStatus == "error" ? "FAIL" : APP_VERSION
    common.sendElasticsearchTrackingVersion(GLOBAL_VARS, namespace_cicd_common, namespace_dev, "dev", sendElsAppVersion, GIT_HASH_OPENSHIFT_CONFIGURATION, trackingVersionList, stageDevPipelineResult, startTimePipeline, endTimePipeline)

  }

  echo 'Start Deploy to QA Environment'

  try {

    startTimePipeline = sdf.format(new Date())

    APP_URL_TYPE_SERVICE = new URL("${GLOBAL_VARS['APP_PROTOCOL']}://${GLOBAL_VARS['APP_NAME']}.${namespace_qa}.svc:${GLOBAL_VARS['APP_PORT']}${GLOBAL_VARS['PATH_INFO']}")

    // Get application version from dev environment
    def resultGetAppVersionDev = restGetVersionApplicationDeploymentConfig {
      appName = GLOBAL_VARS['APP_NAME']
      namespace_env = namespace_dev
    }

    // # if cannot get application version
    if(resultGetAppVersionDev == "") {
      throw "Error: cannot get appVersion when called 'restGetVersionApplicationDeploymentConfig()'"
    }

    APP_VERSION = resultGetAppVersionDev

    // ## STAGE 12 : Deployment Resources For QA Environment
    stage('Deployment Resources For QA Environment') {

      stageCurrentQA = "QA_Deployment-Resources-For-QA-Environment"

      container(name: 'maven') {

        echo 'Deployment Resources for \'qa\' environment was run in \'maven\'container'

        if( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "false" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "false" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "false" ) ) {
          
          try {

            SKIP_QA_ENV.add("false")
            def rcQA = common.qaDeployResToQa(directory_workspace, GLOBAL_VARS, countryCode, openshiftVersion, APP_VERSION, namespace_qa, trackingVersionList, GLOBAL_VARS['GITOPS_REPO_NAME'])

          } catch(e) {
            echo 'Error : Deployment Resources For QA Environment was failed!'
            throw e
          }

        } else {
          echo "SKIP Deployment Resources For QA Environment due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}, DEPLOY_DEV_ONLY = ${GLOBAL_VARS['DEPLOY_DEV_ONLY']}"
        }

      }

    }

    // ## STAGE 13 : Verify application version has changed (QA)
    stage('Verify application version has changed') {

      stageCurrentQA = "QA_Verify-version-application-has-changed"

      container(name: 'maven') {

        echo 'Verify application version for \'qa\' environment was run in \'maven\'container'

        if ( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "false" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "false" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "false" ) ) {

          try {

            SKIP_QA_ENV.add("false")

            acnVerifyVersion {
              global_vars = GLOBAL_VARS
              app_url_openshift_format = APP_URL_TYPE_SERVICE
              version = APP_VERSION // APP_VERSION From DEV
            }

          } catch(e) {

            echo 'Error : Verify application version was failed!'
            throw e

          }

        } else {
          echo "SKIP Verify application version due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}, DEPLOY_DEV_ONLY = ${GLOBAL_VARS['DEPLOY_DEV_ONLY']}"
        }

      }

    }

    // ## STAGE 14 : Run Smoke Test (QA)
    stage('Run Smoke Test') {

      stageCurrentQA = "QA_Run-Smoke-Test"

      container(name: 'tester') {

        echo 'Run Smoke Test for \'qa\' environment was run in \'tester\'container'

        if ( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" ) || ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "system_integration_test" ) || ( RERUN_CONDITION_ACTION == "performance" ) ) {

          echo "SKIP Run smoke test due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}, DEPLOY_DEV_ONLY = ${GLOBAL_VARS['DEPLOY_DEV_ONLY']}"

        } else {

          SKIP_QA_ENV.add("false")
          common.qaRunSmokeTest(directory_workspace, GLOBAL_VARS, RERUN_CONDITION_ACTION, APP_VERSION, APP_URL_TYPE_SERVICE, trackingVersionList)

        }

      }

    }

    // ## STAGE 15 : Run Integration Test (QA)
    stage('Run Integration Test') {

      stageCurrentQA = "QA_Run-Integration-Test"

      container(name: 'tester') {

        echo 'Run integration test for \'qa\' environment was run in \'tester\'container'

        if ( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" ) || ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "system_integration_test" ) || ( RERUN_CONDITION_ACTION == "performance" ) ) {
          
          echo "SKIP Run integration test due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}, DEPLOY_DEV_ONLY = ${GLOBAL_VARS['DEPLOY_DEV_ONLY']}"

        } else {

          SKIP_QA_ENV.add("false")
          common.qaRunIntegrationTest(directory_workspace, GLOBAL_VARS, RERUN_CONDITION_ACTION, APP_VERSION, APP_URL_TYPE_SERVICE, trackingVersionList)

        }

      }

    }

    // ## STAGE 16 : Approve for Promote to Performance
    stage('Approve for Promote to Performance') {

      stageCurrentQA = "QA_Approve-for-promote-to-performance"

      container(name: 'tester') {

        echo 'Approve for Promote to Performance was run in \'tester\'container'

        if ( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" ) || ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "system_integration_test" ) || ( RERUN_CONDITION_ACTION == "performance" ) ) {
        
          echo "SKIP Approve for Promote to Performance due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}, DEPLOY_DEV_ONLY = ${GLOBAL_VARS['DEPLOY_DEV_ONLY']}"

        } else {

          try {

            // Get application version from QA environment
            def resultGetAppVersionQA = restGetVersionApplicationDeploymentConfig {
              appName = GLOBAL_VARS['APP_NAME']
              namespace_env = namespace_qa
            }

            // # if cannot get application version
            if(resultGetAppVersionQA == "") {
              throw "Error: cannot get appVersion when called 'restGetVersionApplicationDeploymentConfig()'"
            }

            APP_VERSION = resultGetAppVersionQA

            SKIP_QA_ENV.add("false")

            def action_approve = approve {
              version = APP_VERSION // APP_VERSION FROM QA
              environment = LIST_ENV[2]
              timeoutTime = 300
            }

            if( action_approve == false ) {
              
              acnSendAlertToWebhook {
                global_vars = GLOBAL_VARS
                envName = LIST_ENV[1]
                stageCurrent = "ABORTED in stage ${stageCurrentQA}"
              }
              
              currentBuild.result = 'ABORTED'

              error "REJECTED promote application to PERFORMANCE environment"
            }

          } catch(e) {
            echo 'Error : Approve for Promote to Performance was failed!'
            throw e
          }

        }

      }

    } // End Stage Approve Promote to Performance

    if(SKIP_DEV_ENV.contains("false")){
      stageQaPipelineResult = "SUCCESS"
    } else {
      stageQaPipelineResult = "Skip all stage in qa environment"
    }

  } catch(e) {

    stageQaStatus = "error"
    stageQaPipelineResult = "FAIL_${stageCurrentQA}"

    error "Pipeline was failed on stage : ${stageCurrentQA} "

    throw e

  } finally {

    endTimePipeline = sdf.format(new Date())

    // SEND PIPELINE RESULT TO ELASTICSEARCH (QA)
    def sendElsAppVersion = stageQaStatus == "error" ? "FAIL" : APP_VERSION
    common.sendElasticsearchTrackingVersion(GLOBAL_VARS, namespace_cicd_common, namespace_qa, "qa", sendElsAppVersion, GIT_HASH_OPENSHIFT_CONFIGURATION, trackingVersionList, stageQaPipelineResult, startTimePipeline, endTimePipeline)

  } // End Finally (QA)

  echo 'Start Deploy to Performance Environment'

  try {

    startTimePipeline = sdf.format(new Date())

    APP_URL_TYPE_SERVICE = new URL("${GLOBAL_VARS['APP_PROTOCOL']}://${GLOBAL_VARS['APP_NAME']}.${namespace_performance}.svc:${GLOBAL_VARS['APP_PORT']}${GLOBAL_VARS['PATH_INFO']}")

    // ## STAGE 17 : Deployment Resources For Performance Environment
    stage('Deployment Resources For Performance Environment') {

      stageCurrentPerformance = "PERFORMANCE_Deployment-Resources-For-PERFORMANCE-Environment"

      container(name: 'maven') {

        echo 'Deployment Resources for \'performance\' environment was run in \'maven\'container'

        if ( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" ) || ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "system_integration_test" ) || ( RERUN_CONDITION_ACTION == "performance" ) ) {
        
          echo "SKIP Deployment Resources For Performance Environment due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}, DEPLOY_DEV_ONLY = ${GLOBAL_VARS['DEPLOY_DEV_ONLY']}"

        } else {

          SKIP_PERFORMANCE_ENV.add("false")

          def rcPerformance = common.perfDeployResToPerf(directory_workspace, GLOBAL_VARS, countryCode, openshiftVersion, APP_VERSION, namespace_performance, trackingVersionList, GLOBAL_VARS['GITOPS_REPO_NAME'])

        }

      }

    }

    // ## STAGE 18 : Verify application version has changed (Performance)
    stage('Verify application version has changed') {

      stageCurrentPerformance = "PERFORMANCE_Verify-version-application-has-changed"

      container(name: 'maven') {

        echo 'Verify application version has changed for \'performance\' environment was run in \'maven\'container'

        if ( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" ) || ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "system_integration_test" ) || ( RERUN_CONDITION_ACTION == "performance" ) ) {
        
          echo "SKIP Verify application version has changed due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}, DEPLOY_DEV_ONLY = ${GLOBAL_VARS['DEPLOY_DEV_ONLY']}"

        } else {

          SKIP_PERFORMANCE_ENV.add("false")

          try {

            acnVerifyVersion {
              global_vars = GLOBAL_VARS
              app_url_openshift_format = APP_URL_TYPE_SERVICE
              version = APP_VERSION // APP_VERSION FROM QA
            }

          } catch(e) {
            
            echo 'Error : Verify application version has changed for \'performance\' environment was failed!'
            throw e

          }

        }

      }

    } // End Stage Verify version

    // // ## STAGE 19 : Run BDD Security (Performance)
    // stage('Run BDD Security') {

    //   stageCurrentPerformance = "PERFORMANCE_Run-BDD-Security"

    //   container(name: 'maven') {

    //     echo 'Run BDD Security for \'performance\' environment was run in \'maven\'container'

    //     if ( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" ) || ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "system_integration_test" ) || ( RERUN_CONDITION_ACTION == "performance" ) ) {
        
    //       echo "SKIP Run BDD Security due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}, DEPLOY_DEV_ONLY = ${GLOBAL_VARS['DEPLOY_DEV_ONLY']}"

    //     } else {

    //       SKIP_PERFORMANCE_ENV.add("false")

    //       sleep(10)

    //     }

    //   }

    // }

    // ## STAGE 20 : Run System Integration Test (Performance)
    stage('Run System Integration Test') {

      stageCurrentPerformance = "PERFORMANCE_Run-system-integration-test"

      container(name: 'tester') {

        echo 'Run System Integration Test for \'performance\' environment was run in \'tester\'container'

        if ( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" ) || ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "performance" ) ) {
        
          echo "SKIP Run system integration test due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}, DEPLOY_DEV_ONLY = ${GLOBAL_VARS['DEPLOY_DEV_ONLY']}"

        } else {

          SKIP_PERFORMANCE_ENV.add("false")
          common.perfRunSystemIntegrationTest(directory_workspace, GLOBAL_VARS, APP_VERSION, RERUN_CONDITION_ACTION, APP_URL_TYPE_SERVICE, trackingVersionList)

        }

      }

    }

    // // Get application version from Performance environment
    // def resultGetAppVersionPerf = restGetVersionApplicationDeploymentConfig {
    //   appName = GLOBAL_VARS['APP_NAME']
    //   namespace_env = namespace_performance
    // }

    // // # if cannot get application version
    // if(resultGetAppVersionPerf == "") {
    //   throw "Error: cannot get appVersion when called 'restGetVersionApplicationDeploymentConfig()'"
    // }

    // APP_VERSION = resultGetAppVersionPerf

    // ## STAGE 21 : Run Performance Test (Performance)
    stage('Run Performance Test') {

      stageCurrentPerformance = "PERFORMANCE_run-performance-test"

      container(name: 'tester') {

        echo 'Run System Integration Test for \'performance\' environment was run in \'tester\'container'

        if ( ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" ) || ( RERUN_CONDITION_ACTION == "ignore" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "true" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( RERUN_CONDITION_ACTION == "dev" && BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY == "false" && GLOBAL_VARS['DEPLOY_DEV_ONLY'] == "true" ) || ( SKIP_RUN_PERFORMANCE_TEST == "true" ) ) {
        
          echo "SKIP Run Performance Test due to rerun condition is ${RERUN_CONDITION_ACTION}, BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY = ${BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY}, DEPLOY_DEV_ONLY = ${GLOBAL_VARS['DEPLOY_DEV_ONLY']}"

        } else {

          SKIP_PERFORMANCE_ENV.add("false")
          common.perfRunPerformanceTest(directory_workspace, GLOBAL_VARS, trackingVersionList)

        }

      }

    }

    if(SKIP_PERFORMANCE_ENV.contains("false")){
      stagePerformancePipelineResult = "SUCCESS"
    } else {
      stagePerformancePipelineResult = "Skip all stage in performance environment"
    }

  } catch(e) {

    stagePerformanceStatus = "error"
    stagePerformancePipelineResult = "FAIL_${stageCurrentPerformance}"

    error "Pipeline was failed on stage : ${stageCurrentPerformance} "
    
    throw e

  } finally {

    endTimePipeline = sdf.format(new Date())

    // SEND PIPELINE RESULT TO ELASTICSEARCH (PERFORMANCE)
    def sendElsAppVersion = stagePerformanceStatus == "error" ? "FAIL" : APP_VERSION
    common.sendElasticsearchTrackingVersion(GLOBAL_VARS, namespace_cicd_common, namespace_performance, "performance", sendElsAppVersion, GIT_HASH_OPENSHIFT_CONFIGURATION, trackingVersionList, stagePerformancePipelineResult, startTimePipeline, endTimePipeline)

  } // End Finally

} // End Scope mavenNode