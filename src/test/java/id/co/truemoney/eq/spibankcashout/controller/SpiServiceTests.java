package id.co.truemoney.eq.spibankcashout.controller;

import com.ascendmoney.equator.sdk.payment.model.beans.OrderReference;
import com.ascendmoney.equator.sdk.spi.helper.ResponseHelper;
import com.ascendmoney.equator.sdk.spi.model.PostPaymentRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import equator.sdk.integration.autoconfigure.payment.EquatorPaymentAutoConfiguration;
import id.co.truemoney.eq.spibankcashout.controller.base.BaseControllerTest;
import id.co.truemoney.eq.spibankcashout.services.SpiService;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(value = {RestDocumentationExtension.class, SpringExtension.class})
@EnableAutoConfiguration(exclude = {RabbitAutoConfiguration.class, EquatorPaymentAutoConfiguration.class})
@Log4j2
public class SpiServiceTests extends BaseControllerTest {

    @MockBean
    private SpiService spiService;

    @Test
    public void testPostPayment() throws Exception {

        List<OrderReference> orderReferenceList = new ArrayList<>();
        OrderReference orderReference = new OrderReference();
        orderReference.setKey("serviceProviderName");
        orderReference.setValue("Globe");
        orderReferenceList.add(orderReference);

        orderReference = new OrderReference();
        orderReference.setKey("transactionRefNo");
        orderReference.setValue("0906105T00SA");
        orderReferenceList.add(orderReference);

        orderReference = new OrderReference();
        orderReference.setKey("mobileNo");
        orderReference.setValue("09999999999");
        orderReferenceList.add(orderReference);

        orderReference = new OrderReference();
        orderReference.setKey("productCode");
        orderReference.setValue("Test Product Code");
        orderReferenceList.add(orderReference);

        orderReference = new OrderReference();
        orderReference.setKey("amount");
        orderReference.setValue("10.00");
        orderReferenceList.add(orderReference);

        orderReference = new OrderReference();
        orderReference.setKey("referenceId");
        orderReference.setValue("test Reference ID");
        orderReferenceList.add(orderReference);

        orderReference = new OrderReference();
        orderReference.setKey("responseData");
        orderReference.setValue("INI MOCK");
        orderReferenceList.add(orderReference);

        spiRequestDetail.setOrderReferences(orderReferenceList);

        Map<String, Object> requestMapData = new HashMap<>();
        requestMapData.put("data", spiRequestDetail);

        when(spiService.onPostPayment(any(PostPaymentRequest.class)))
                .thenReturn(ResponseHelper.response("success", "OK", spiRequestDetail));

        mockMvc.perform(post("/receive/post-payment")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectToJson(requestMapData)))
                .andExpect(status().isOk())
                .andDo(document("globe-postpayment",
                        Preprocessors.preprocessRequest(Preprocessors.prettyPrint()),
                        Preprocessors.preprocessResponse(Preprocessors.prettyPrint())
                ));
    }

    private String objectToJson(Object object){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            return objectMapper.writeValueAsString( object );
        } catch (JsonProcessingException var2) {
            log.debug( var2.getMessage(), var2 );
            log.error( "e: ", var2 );
        } catch (Exception var3) {
            log.debug( var3.getMessage(), var3 );
            log.error( "e: ", var3 );
        }

        return null;
    }
}
