package id.co.truemoney.eq.spibankcashout.controller.base;

import com.ascendmoney.equator.sdk.core.model.Status;
import com.ascendmoney.equator.sdk.spi.controller.ReceivePaymentEndpoint;
import com.ascendmoney.equator.sdk.spi.model.SPIRequestDetail;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;

public abstract class BaseControllerTest {

    @Autowired
    protected ReceivePaymentEndpoint receivePaymentEndpoint;
    protected MockMvc mockMvc;
    protected SPIRequestDetail spiRequestDetail;
    protected SPIRequestDetail spiResponseDetail;
    protected Status statusSuccess;

    @BeforeEach
    public void init(RestDocumentationContextProvider restDocumentation) throws JsonProcessingException {
        mockMvc = MockMvcBuilders.standaloneSetup(receivePaymentEndpoint)
                .apply(documentationConfiguration(restDocumentation))
                .build();
        spiRequestDetail = new SPIRequestDetail();
        spiRequestDetail.setOrderId("19072507353102366115051");
        spiResponseDetail = new SPIRequestDetail();
        spiResponseDetail.setOrderId("19072507353102366115051");
        statusSuccess = new Status();
        statusSuccess.setCode("success");
        statusSuccess.setMessage("success");
    }

}
