package id.co.truemoney.eq.spibankcashout;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.truemoney.eq.spibankcashout.domain.http.pojo.ComplianceData;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Sender;

@SpringBootTest
class BankSimulatorServiceTests {

	@Autowired
	private ObjectMapper objectMapper;
	@Test
	void test() {
		String json = "{\r\n" + 
				"   \"name\":{\r\n" + 
				"      \"fullName\":\"Raja Irfan\",\r\n" + 
				"      \"firstName\":\"Raja\",\r\n" + 
				"      \"lastName\":\"Irfan\"\r\n" + 
				"   },\r\n" + 
				"   \"address\":{\r\n" + 
				"      \"address\":\"Kp Bantar Muncang Atas RT 02 RW 05, Desa Sekarwangi- Cibadak\",\r\n" + 
				"      \"city\":\"Sukabumi\",\r\n" + 
				"      \"countryIsoCode\":\"ID\",\r\n" + 
				"      \"postalCode\":\"43351\"\r\n" + 
				"   },\r\n" + 
				"   \"complianceDetails\":{\r\n" + 
				"      \"type\":\"NATIONAL_ID\",\r\n" + 
				"      \"number\":\"3202110404860010\",\r\n" + 
				"      \"issuePlace\":\"SUKABUMI\",\r\n" + 
				"      \"issueCountry\":\"ID\",\r\n" + 
				"      \"expires\":\"NE\",\r\n" + 
				"      \"documentVerified\":\"TRUE\"\r\n" + 
				"   },\r\n" + 
				"   \"mobilePhone\":{\r\n" + 
				"      \"countryCode\":\"62\",\r\n" + 
				"      \"number\":\"87889479577\"\r\n" + 
				"   },\r\n" + 
				"   \"gender\":\"M\",\r\n" + 
				"   \"dateOfBirth\":\"1986-04-04\",\r\n" + 
				"   \"placeOfBirth\":\"Jakarta\",\r\n" + 
				"   \"occupation\":\"PEKERJA SWASTA\",\r\n" + 
				"   \"sourceOfFund\":\"SALARY\",\r\n" + 
				"   \"purpose\":\"OTHER\",\r\n" + 
				"   \"relationShip\":\"FAMILY\"\r\n" + 
				"}";
		
		Sender sender = new Sender();
		try {
			sender = objectMapper.readValue(json, Sender.class);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("Raja", sender.getName().getFirstName());
	
	}
	
	@Test
	void test2() {
		String json = "{\r\n" + 
				"   \"firstName\":\"Raja\",\r\n" + 
				"   \"lastName\":\"Irfan\",\r\n" + 
				"   \"address\":\"Kp Bantar Muncang Atas RT 02 RW 05, Desa Sekarwangi- Cibadak\",\r\n" + 
				"   \"city\":\"Sukabumi\",\r\n" + 
				"   \"postalCode\":\"43351\",\r\n" + 
				"   \"idType\":\"NATIONAL_ID\",\r\n" + 
				"   \"idNo\":\"3202110404860010\",\r\n" + 
				"   \"issuedPlace\":\"SUKABUMI\",\r\n" + 
				"   \"country\":\"ID\",\r\n" + 
				"   \"expiredDate\":\"NE\",\r\n" + 
				"   \"countryCode\":\"62\",\r\n" + 
				"   \"gender\":\"M\",\r\n" + 
				"   \"dob\":\"1986-04-04\",\r\n" + 
				"   \"pob\":\"Jakarta\",\r\n" + 
				"   \"occupation\":\"PEKERJA SWASTA\"\r\n" + 
				"}";
		 ComplianceData cd = new ComplianceData();
		 
		 try {
		 cd = objectMapper.readValue(json, ComplianceData.class);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 assertEquals("Raja", cd.getFirstName());
	}

}
