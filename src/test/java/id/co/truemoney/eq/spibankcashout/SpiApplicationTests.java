package id.co.truemoney.eq.spibankcashout;

import equator.sdk.integration.autoconfigure.payment.ReceivePaymentAutoConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = {ReceivePaymentAutoConfiguration.class})
class SpiApplicationTests {

    @Test
    void contextLoads() {
    }

}
