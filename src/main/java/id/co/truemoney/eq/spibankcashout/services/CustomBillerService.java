package id.co.truemoney.eq.spibankcashout.services;

import com.ascendmoney.equator.sdk.core.model.BaseEquatorResponse;
import com.ascendmoney.equator.sdk.spi.exception.BillerManagementException;
import com.ascendmoney.equator.sdk.spi.helper.ResponseHelper;
import com.ascendmoney.equator.sdk.spi.model.bill.request.billpayment.BillPostPaymentRequest;
import com.ascendmoney.equator.sdk.spi.model.bill.request.provideramount.GetAmountRequest;
import com.ascendmoney.equator.sdk.spi.service.ReceiveBillerManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CustomBillerService implements ReceiveBillerManagementService {

    @Autowired
    private BankSimulatorService bankSimulatorService;
	@Autowired
    private  ObjectMapper objectMapper;

	
    @Override
    public <S extends BaseEquatorResponse> ResponseEntity<S> getProviderAmount(GetAmountRequest request) throws BillerManagementException {
        try {
			log.debug("getProviderAmount {}", objectMapper.writeValueAsString(request));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return null;
    }

    @Override
    public <S extends BaseEquatorResponse> ResponseEntity<S> getBillsPrePayment(BillPostPaymentRequest request) throws BillerManagementException {
    	try {
        log.debug("getBillsPrePayment {}", objectMapper.writeValueAsString(request));
        		return bankSimulatorService.getBillsPrePayment(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    
    }

    @Override
    public <S extends BaseEquatorResponse> ResponseEntity<S> getBillsPostPayment(BillPostPaymentRequest request) throws BillerManagementException {
        log.debug("getBillsPostPayment {}", request.toString());
        return null;
    }
}
