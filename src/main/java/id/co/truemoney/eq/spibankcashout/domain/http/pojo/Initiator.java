package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

public class Initiator {

	private String countryCode;
	private String accountNo;
	
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	
	
	
}
