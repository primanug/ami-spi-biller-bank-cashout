package id.co.truemoney.eq.spibankcashout.web.dto;

import java.math.BigDecimal;
import java.util.List;

import com.ascendmoney.equator.sdk.payment.model.beans.OrderReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GetAmountResponseData {

    private List<OrderReference> billReferences;
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public List<OrderReference> getBillReferences() {
        return billReferences;
    }

    public void setBillReferences(List<OrderReference> billReferences) {
        this.billReferences = billReferences;
    }

}
