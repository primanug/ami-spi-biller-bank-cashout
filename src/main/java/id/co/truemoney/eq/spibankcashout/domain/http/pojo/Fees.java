package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Valid
public class Fees {
	private String currencyIsoCode;
	private double charges;
	@NotNull
	private double exchangeFee;
	@NotNull
	private double discount;
}
