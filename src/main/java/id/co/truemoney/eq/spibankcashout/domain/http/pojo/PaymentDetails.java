package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Valid
public class PaymentDetails {

	@Valid
	@NotNull
	private Origination origination;
	@Valid
	@NotNull
	private Destination destination;
	@Valid
	@NotNull
	private Fees fees;
	private String promoCode;
	private String customerRefNumber;
	@NotNull
	private String transactionTime;
}
