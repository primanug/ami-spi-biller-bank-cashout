package id.co.truemoney.eq.spibankcashout.domain.http;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import id.co.truemoney.eq.spibankcashout.configs.OpenFeignConfig;
import id.co.truemoney.eq.spibankcashout.domain.http.request.BankCashoutCommitRequest;
import id.co.truemoney.eq.spibankcashout.domain.http.request.BankCashoutInquiryRequest;
import id.co.truemoney.eq.spibankcashout.domain.http.response.BankCashoutCommitResponse;
import id.co.truemoney.eq.spibankcashout.domain.http.response.BankCashoutInquiryResponse;

@FeignClient(value = "alto-mcpay-gateway", url = "http://172.16.50.154:40030", configuration = OpenFeignConfig.class)
public interface AltoMcPayService {

	@RequestMapping(method = RequestMethod.POST, value = "/bank_inquiry")
	public BankCashoutInquiryResponse inquiry(BankCashoutInquiryRequest bankCashoutInquiryRequest);
	@RequestMapping(method = RequestMethod.POST, value = "/bank_commit")
	public BankCashoutCommitResponse commit(BankCashoutCommitRequest bankCashoutCommitRequest);
	
	
}
