package id.co.truemoney.eq.spibankcashout.services;

import com.ascendmoney.equator.sdk.payment.model.beans.OrderReference;
import com.ascendmoney.equator.sdk.spi.helper.ResponseHelper;
import com.ascendmoney.equator.sdk.spi.model.NotificationRequest;
import com.ascendmoney.equator.sdk.spi.model.PostPaymentRequest;
import com.ascendmoney.equator.sdk.spi.model.PreOrderRequest;
import com.ascendmoney.equator.sdk.spi.model.PrePaymentRequest;
import com.ascendmoney.equator.sdk.spi.service.ReceivePaymentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class SpiService implements ReceivePaymentService {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BankSimulatorService bankSimulatorService;
    
    @Override
    public ResponseEntity onPreOrder(PreOrderRequest request) throws Exception {
        // --> For demo purpose
        String data = objectMapper.writeValueAsString(request);
        log.debug("onPreOrder ------------------------------------------");
        log.debug(data);
        if(request.getRequestDetail().getProductService().getId()==6) {
        	return bankSimulatorService.onPreOrder(request);
        }
        return ResponseHelper
                .response(String.valueOf(HttpStatus.OK.value()), "success", request.getRequestDetail(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity onPrePayment(PrePaymentRequest request) throws Exception {
        String data = objectMapper.writeValueAsString(request);
        log.debug("onPrePayment ------------------------------------------");
        log.debug(data);
        return ResponseHelper
                .response(String.valueOf(HttpStatus.OK.value()), "success", request.getRequestDetail(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity onPostPayment(PostPaymentRequest request) throws Exception {
        OrderReference orderReference = new OrderReference();
        orderReference.setKey("responseData");
        orderReference.setValue("{\n" +
                "    \"externalRefNumber\": \"1606806299\",\n" +
                "    \"transactionId\": \"1003996112\",\n" +
                "    \"serviceName\": \"BILLER_COMMIT\",\n" +
                "    \"messageResponse\": {\n" +
                "        \"code\": 0,\n" +
                "        \"description\": \"OK\"\n" +
                "    }}");
        request.getRequestDetail().getOrderReferences().add(orderReference);

        log.debug("onPostPayment ------------------------------------------");
        String data = objectMapper.writeValueAsString(request);
        log.debug(data);

        log.debug("PRODUCT {}", request.getRequestDetail().getProduct());
        if(request.getRequestDetail().getProduct().getName().equals("PRD_PENDING")){
            log.debug("PRD_PENDING");
            request.getRequestDetail().setStatus(1);
        }

        return ResponseHelper
                .response(String.valueOf(HttpStatus.OK.value()), "success", request.getRequestDetail(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity onNotification(NotificationRequest request) throws Exception {
        log.debug("onNotification ------------------------------------------");
        String data = objectMapper.writeValueAsString(request);
        log.debug(data);
        return ResponseHelper
                .response(String.valueOf(HttpStatus.OK.value()), "success", request.getRequestDetail(), HttpStatus.OK);
    }
}
