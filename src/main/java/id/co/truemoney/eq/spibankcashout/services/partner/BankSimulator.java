package id.co.truemoney.eq.spibankcashout.services.partner;

import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Account;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.ComplianceData;
import id.co.truemoney.eq.spibankcashout.domain.http.request.BankCashoutCommitRequest;
import id.co.truemoney.eq.spibankcashout.domain.http.request.BankCashoutInquiryRequest;
import id.co.truemoney.eq.spibankcashout.domain.http.response.BankCashoutCommitResponse;
import id.co.truemoney.eq.spibankcashout.domain.http.response.BankCashoutInquiryResponse;

public interface BankSimulator {

	public BankCashoutInquiryResponse inquiry(String transactionId, BankCashoutInquiryRequest inquiryRequest, Account senderAccount, ComplianceData senderComplianceData);
	public BankCashoutCommitResponse commit(String transactionId, BankCashoutCommitRequest bankCashoutCommitRequest, Account senderAccount, ComplianceData senderComplianceData);
}
