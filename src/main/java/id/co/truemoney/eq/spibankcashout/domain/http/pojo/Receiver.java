/**
 * Project    : International Remittance - Single API 
 * Created By : Megi Permana
 * Date       : Jul 24, 2018
 * Time       : 12:30:10 PM 
 */
package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class Receiver {

	@Valid
	@NotNull
	private Name name;
	private Address address;
	private ComplianceDetails complianceDetails;
	private MobilePhone mobilePhone;
	private String gender;
	private String dateOfBirth;
	private String placeOfBirth;
	private String occupation;
	
	/**
	 * @return the name
	 */
	public Name getName() {
		if(name == null)
			name = new Name();
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(Name name) {
		this.name = name;
	}
	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	/**
	 * @return the complianceDetails
	 */
	public ComplianceDetails getComplianceDetails() {
		return complianceDetails;
	}
	/**
	 * @param complianceDetails the complianceDetails to set
	 */
	public void setComplianceDetails(ComplianceDetails complianceDetails) {
		this.complianceDetails = complianceDetails;
	}
	/**
	 * @return the mobilePhone
	 */
	public MobilePhone getMobilePhone() {
		return mobilePhone;
	}
	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(MobilePhone mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	/**
	 * @return the placeOfBirth
	 */
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	/**
	 * @param placeOfBirth the placeOfBirth to set
	 */
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}
	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
		
}
