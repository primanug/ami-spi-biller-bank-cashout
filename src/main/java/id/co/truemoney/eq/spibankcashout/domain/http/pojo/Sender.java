/**
 * Project    : International Remittance - Single API 
 * Created By : Megi Permana
 * Date       : Jul 24, 2018
 * Time       : 12:30:10 PM 
 */
package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Valid
public class Sender {

	@Valid
	@NotNull
	private Name name;
	@Valid
	@NotNull
	private Address address;
	@Valid
	@NotNull
	private ComplianceDetails complianceDetails;
	@Valid
	@NotNull
	private MobilePhone mobilePhone;
	@NotNull
	@NotBlank
	private String gender;
	@NotNull
	@NotBlank
	private String dateOfBirth;
	@NotNull
	@NotBlank
	private String placeOfBirth;
	@NotNull
	@NotBlank
	private String occupation;
	@NotNull
	@NotBlank
	private String sourceOfFund;
	@NotNull
	@NotBlank
	private String purpose;
	@NotNull
	@NotBlank
	private String relationShip;
}
