/**
 * Project    : International Remittance - Single API 
 * Created By : Megi Permana
 * Date       : Jul 24, 2018
 * Time       : 12:30:10 PM 
 */
package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Valid
public class Destination {

	@NotNull
	@NotBlank
	private String currencyIsoCode;
	private String countryIsoCode;
	private String stateCode;
	@NotNull
	@NotBlank
	private String expectedPayoutMethod;
	@NotNull
	private long expectedPayoutAmount;
	private String accountNumber;
	private String issuerCode;
	private String issuerName;
	
	
	/**
	 * @return the issuerCode
	 */
	public String getIssuerCode() {
		return issuerCode;
	}
	/**
	 * @param issuerCode the issuerCode to set
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	/**
	 * @return the currencyIsoCode
	 */
	public String getCurrencyIsoCode() {
		return currencyIsoCode;
	}
	/**
	 * @param currencyIsoCode the currencyIsoCode to set
	 */
	public void setCurrencyIsoCode(String currencyIsoCode) {
		this.currencyIsoCode = currencyIsoCode;
	}
	/**
	 * @return the countryIsoCode
	 */
	public String getCountryIsoCode() {
		return countryIsoCode;
	}
	/**
	 * @param countryIsoCode the countryIsoCode to set
	 */
	public void setCountryIsoCode(String countryIsoCode) {
		this.countryIsoCode = countryIsoCode;
	}
	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}
	/**
	 * @param stateCode the stateCode to set
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	/**
	 * @return the expectedPayoutMethod
	 */
	public String getExpectedPayoutMethod() {
		return expectedPayoutMethod;
	}
	/**
	 * @param expectedPayoutMethod the expectedPayoutMethod to set
	 */
	public void setExpectedPayoutMethod(String expectedPayoutMethod) {
		this.expectedPayoutMethod = expectedPayoutMethod;
	}
	/**
	 * @return the expectedPayoutAmount
	 */
	public long getExpectedPayoutAmount() {
		return expectedPayoutAmount;
	}
	/**
	 * @param expectedPayoutAmount the expectedPayoutAmount to set
	 */
	public void setExpectedPayoutAmount(long expectedPayoutAmount) {
		this.expectedPayoutAmount = expectedPayoutAmount;
	}
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}
	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	
}
