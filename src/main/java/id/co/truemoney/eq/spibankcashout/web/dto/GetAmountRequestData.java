package id.co.truemoney.eq.spibankcashout.web.dto;

import com.ascendmoney.equator.sdk.payment.model.beans.OrderReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.List;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class GetAmountRequestData {

    private List<OrderReference> billReferences;
    private String partnerRefNumber;
    private String customerFirstName;
    private String customerLastName;
    private String customerMiddleName;
    private String mobileNumber;
    private Provider provider;
}
