/**
 * Project    : International Remittance - Single API 
 * Created By : Megi Permana
 * Date       : Jul 24, 2018
 * Time       : 12:30:10 PM 
 */
package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ComplianceDetails {

	@NotNull
	@NotBlank
	private String type;
	@NotNull
	@NotBlank
	private String number;
	@NotNull
	@NotBlank
	private String issuePlace;
	@NotNull
	@NotBlank
	private String issueCountry;
	@NotNull
	@NotBlank
	private String expires;
	private String expireDate;
	private String nationality;
	@NotNull
	@NotBlank
	private String documentVerified;

}
