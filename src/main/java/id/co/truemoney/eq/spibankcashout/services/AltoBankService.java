package id.co.truemoney.eq.spibankcashout.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ascendmoney.equator.sdk.payment.model.beans.OrderReference;
import com.ascendmoney.equator.sdk.spi.helper.ResponseHelper;
import com.ascendmoney.equator.sdk.spi.model.PreOrderRequest;
import com.ascendmoney.equator.sdk.spi.service.ReceivePaymentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Destination;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.PaymentDetails;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Sender;
import id.co.truemoney.eq.spibankcashout.domain.http.request.BankCashoutInquiryRequest;
import id.co.truemoney.eq.spibankcashout.domain.http.response.BankCashoutInquiryResponse;
import id.co.truemoney.eq.spibankcashout.services.partner.Alto;

@Service
public class AltoBankService {

	private final Alto alto;
    private ObjectMapper objectMapper;
	
	public AltoBankService(Alto alto,ObjectMapper objectMapper) {
		this.alto = alto;
		this.objectMapper = objectMapper;
	}

//	@Override
	public ResponseEntity onPreOrder(PreOrderRequest request) throws Exception {
		BankCashoutInquiryRequest inquiryRequest = new BankCashoutInquiryRequest();
		inquiryRequest.setSender(defaultSender());
		inquiryRequest.setPaymentDetails(new PaymentDetails());
		inquiryRequest.getPaymentDetails().setDestination(new Destination());
		
		inquiryRequest.setExternalRefNumber(request.getExtTransactionId());
		inquiryRequest.getPaymentDetails().getDestination().setAccountNumber(request.getRequestDetail().getProduct().getRef2());
		inquiryRequest.getPaymentDetails().getDestination().setExpectedPayoutAmount(Long.valueOf(request.getRequestDetail().getAmount().longValue()));
		inquiryRequest.getPaymentDetails().getDestination().setIssuerCode(request.getRequestDetail().getProduct().getRef4());
		//set payment detail
		BankCashoutInquiryResponse response = alto.inquiry(request.getOrderId(), inquiryRequest, null, null);
		OrderReference orderRef = new OrderReference();
		orderRef.setKey("Receiver");
		String json = objectMapper.writeValueAsString(response.getReceiver());
		orderRef.setValue(json);
        request.getRequestDetail().getOrderReferences().add(orderRef);
		return ResponseHelper
                .response(String.valueOf(HttpStatus.OK.value()), "success", request, HttpStatus.OK);
    	}
	
	
	public Sender defaultSender() throws JsonMappingException, JsonProcessingException {
		String json = "{\r\n" + 
				"\"name\": {\r\n" + 
				"\"fullName\": \"Raja Irfan\",\r\n" + 
				"\"firstName\": \"Raja\",\r\n" + 
				"\"lastName\": \"Irfan\"\r\n" + 
				"},\r\n" + 
				"\"address\": {\r\n" + 
				"\"address\": \"Kp Bantar Muncang Atas RT 02 RW 05, Desa Sekarwangi- Cibadak\",\r\n" + 
				"\"city\": \"Sukabumi\",\r\n" + 
				"\"countryIsoCode\": \"ID\",\r\n" + 
				"\"postalCode\": \"43351\"\r\n" + 
				"},\r\n" + 
				"\"complianceDetails\": {\r\n" + 
				"\"type\": \"NATIONAL_ID\",\r\n" + 
				"\"number\": \"3202110404860010\",\r\n" + 
				"\"issuePlace\": \"SUKABUMI\",\r\n" + 
				"\"issueCountry\": \"ID\",\r\n" + 
				"\"expires\": \"NE\",\r\n" + 
				"\"documentVerified\": \"TRUE\"\r\n" + 
				"},\r\n" + 
				"\"mobilePhone\": {\r\n" + 
				"\"countryCode\": \"62\",\r\n" + 
				"\"number\": \"87889479577\"\r\n" + 
				"},\r\n" + 
				"\"gender\": \"M\",\r\n" + 
				"\"dateOfBirth\": \"1986-04-04\",\r\n" + 
				"\"placeOfBirth\": \"Jakarta\",\r\n" + 
				"\"occupation\": \"PEKERJA SWASTA\",\r\n" + 
				"\"sourceOfFund\": \"SALARY\",\r\n" + 
				"\"purpose\": \"OTHER\",\r\n" + 
				"\"relationShip\": \"FAMILY\"\r\n" + 
				"}";
		return objectMapper.readValue(json, Sender.class);
		
	}
}
