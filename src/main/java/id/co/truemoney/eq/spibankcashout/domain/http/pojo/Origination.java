/**
 * Project    : International Remittance - Single API 
 * Created By : Megi Permana
 * Date       : Jul 24, 2018
 * Time       : 12:30:10 PM 
 */
package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Valid
public class Origination {

	private double principalAmount;
	private double grossAmount;
	@NotNull
	@NotBlank
	private String currencyIsoCode;
	@NotNull
	@NotBlank
	private String countryIsoCode;
	/**
	 * @return the principalAmount
	 */
	public double getPrincipalAmount() {
		return principalAmount;
	}
	/**
	 * @param principalAmount the principalAmount to set
	 */
	public void setPrincipalAmount(double principalAmount) {
		this.principalAmount = principalAmount;
	}
	/**
	 * @return the currencyIsoCode
	 */
	public String getCurrencyIsoCode() {
		return currencyIsoCode;
	}
	/**
	 * @param currencyIsoCode the currencyIsoCode to set
	 */
	public void setCurrencyIsoCode(String currencyIsoCode) {
		this.currencyIsoCode = currencyIsoCode;
	}
	/**
	 * @return the countryIsoCode
	 */
	public String getCountryIsoCode() {
		return countryIsoCode;
	}
	/**
	 * @param countryIsoCode the countryIsoCode to set
	 */
	public void setCountryIsoCode(String countryIsoCode) {
		this.countryIsoCode = countryIsoCode;
	}
	/**
	 * @return the grossAmount
	 */
	public double getGrossAmount() {
		return grossAmount;
	}
	/**
	 * @param grossAmount the grossAmount to set
	 */
	public void setGrossAmount(double grossAmount) {
		this.grossAmount = grossAmount;
	}
	
	
	
}
