/**
 * Project    : International Remittance - Single API 
 * Created By : Megi Permana
 * Date       : Jul 24, 2018
 * Time       : 12:30:10 PM 
 */
package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Valid
public class Address {
	@NotNull
	@NotBlank
	private String address;
	@NotNull
	@NotBlank
	private String city;
	private String state;
	@NotNull
	@NotBlank
	private String countryIsoCode;
	@NotNull
	@NotBlank
	private String postalCode;
}
