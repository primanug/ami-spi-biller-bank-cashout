package id.co.truemoney.eq.spibankcashout.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.truemoney.eq.spibankcashout.services.BankSimulatorService;
import id.co.truemoney.eq.spibankcashout.web.dto.GetAmountRequest;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/biller-management")
@Slf4j
public class BillerManagementEndpoints {
	@Autowired
	private BankSimulatorService bankSimulatorService;
	
	
    @PostMapping(
            path = {"/providers/amounts"}
    )
    public ResponseEntity getAmounts(@RequestBody GetAmountRequest request) throws Exception {
        log.debug("getAmounts {}", request.toString());
        return bankSimulatorService.getProviderAmounts(request);
    }
}
