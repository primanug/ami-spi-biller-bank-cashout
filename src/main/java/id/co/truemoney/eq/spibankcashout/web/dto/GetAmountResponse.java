package id.co.truemoney.eq.spibankcashout.web.dto;

import com.ascendmoney.equator.sdk.core.model.BaseEquatorResponse;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GetAmountResponse extends BaseEquatorResponse<GetAmountResponseData> {
}
