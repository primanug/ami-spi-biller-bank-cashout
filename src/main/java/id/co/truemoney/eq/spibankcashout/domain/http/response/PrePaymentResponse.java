package id.co.truemoney.eq.spibankcashout.domain.http.response;

import com.ascendmoney.equator.sdk.core.model.BaseEquatorResponse;
import com.ascendmoney.equator.sdk.spi.model.SPIRequestDetail;

import lombok.Data;

@Data
public class PrePaymentResponse extends BaseEquatorResponse<SPIRequestDetail>{

	
}
