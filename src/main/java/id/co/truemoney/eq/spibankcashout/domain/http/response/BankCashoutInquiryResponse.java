package id.co.truemoney.eq.spibankcashout.domain.http.response;

import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Partner;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.PaymentDetails;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Receiver;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Sender;

public class BankCashoutInquiryResponse {

	private Partner partner;
	private String externalRefNumber;
	private String transactionId;
	private String partnerTransactionId;
	private String productId;
	private Sender sender;
	private Receiver receiver;
	private PaymentDetails paymentDetails;
	private MessageResponse messageResponse;
	
	public String getPartnerTransactionId() {
		return partnerTransactionId;
	}
	public void setPartnerTransactionId(String partnerTransactionId) {
		this.partnerTransactionId = partnerTransactionId;
	}
	public Partner getPartner() {
		return partner;
	}
	public void setPartner(Partner partner) {
		this.partner = partner;
	}
	public String getExternalRefNumber() {
		return externalRefNumber;
	}
	public void setExternalRefNumber(String externalRefNumber) {
		this.externalRefNumber = externalRefNumber;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public Sender getSender() {
		return sender;
	}
	public void setSender(Sender sender) {
		this.sender = sender;
	}
	public Receiver getReceiver() {
		return receiver;
	}
	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}
	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	public MessageResponse getMessageResponse() {
		return messageResponse;
	}
	public void setMessageResponse(MessageResponse messageResponse) {
		this.messageResponse = messageResponse;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
}
