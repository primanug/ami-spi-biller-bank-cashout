/**
 * Project    : International Remittance - Single API 
 * Created By : Megi Permana
 * Date       : Jul 24, 2018
 * Time       : 12:30:10 PM 
 */
package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class Name {

	@NotNull
	@NotBlank
	private String fullName;
	@NotNull
	@NotBlank
	private String firstName;
	@NotNull
	@NotBlank
	private String lastName;
	
}
