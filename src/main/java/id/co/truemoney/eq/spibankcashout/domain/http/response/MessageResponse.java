/**
 * Project    : International Remittance - Single API 
 * Created By : Megi Permana
 * Date       : Jul 27, 2018
 * Time       : 4:20:22 PM 
 */
package id.co.truemoney.eq.spibankcashout.domain.http.response;

public class MessageResponse {

	private int code;
	private String description;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
