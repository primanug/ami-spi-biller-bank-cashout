package id.co.truemoney.eq.spibankcashout.services;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ascendmoney.equator.sdk.payment.model.beans.OrderReference;
import com.ascendmoney.equator.sdk.spi.helper.ResponseHelper;
import com.ascendmoney.equator.sdk.spi.model.PreOrderRequest;
import com.ascendmoney.equator.sdk.spi.model.SPIRequestDetail;
import com.ascendmoney.equator.sdk.spi.model.bill.request.billpayment.BillPostPaymentRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Account;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.ComplianceData;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Destination;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Fees;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Origination;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.PaymentDetails;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Sender;
import id.co.truemoney.eq.spibankcashout.domain.http.request.BankCashoutCommitRequest;
import id.co.truemoney.eq.spibankcashout.domain.http.request.BankCashoutInquiryRequest;
import id.co.truemoney.eq.spibankcashout.domain.http.response.BankCashoutCommitResponse;
import id.co.truemoney.eq.spibankcashout.domain.http.response.BankCashoutInquiryResponse;
import id.co.truemoney.eq.spibankcashout.domain.http.response.MessageResponse;
import id.co.truemoney.eq.spibankcashout.services.partner.BankSimulator;
import id.co.truemoney.eq.spibankcashout.web.dto.GetAmountRequest;
import id.co.truemoney.eq.spibankcashout.web.dto.GetAmountResponseData;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class BankSimulatorService {

	private final BankSimulator bankSimulator;
	private final ObjectMapper objectMapper;
	public BankSimulatorService(BankSimulator bankSimulator, ObjectMapper objectMapper) {
		this.bankSimulator = bankSimulator;
		this.objectMapper = objectMapper;
	}
	
	public ResponseEntity onPreOrder(PreOrderRequest request) throws Exception {
		BankCashoutInquiryRequest inquiryRequest = new BankCashoutInquiryRequest();
		inquiryRequest.setSender(defaultSender());
		inquiryRequest.setPaymentDetails(new PaymentDetails());
		inquiryRequest.getPaymentDetails().setDestination(new Destination());
		inquiryRequest.setExternalRefNumber(request.getExtTransactionId());
		inquiryRequest.getPaymentDetails().getDestination().setAccountNumber(request.getRequestDetail().getProduct().getRef2());
		inquiryRequest.getPaymentDetails().getDestination().setExpectedPayoutAmount(request.getRequestDetail().getAmount().longValue());
		inquiryRequest.getPaymentDetails().getDestination().setIssuerCode(request.getRequestDetail().getProduct().getRef4());
		//set payment detail 
		log.debug("json request {}",objectMapper.writeValueAsString(inquiryRequest));
		
		BankCashoutInquiryResponse response = bankSimulator.inquiry(request.getOrderId(), inquiryRequest,defaultSenderAccount(), defaultCompData());
		String json = objectMapper.writeValueAsString(response);
		log.debug("json return {}",json);
		OrderReference orderRef = new OrderReference();
		orderRef.setKey("bankResponse");
		orderRef.setValue(json);
		
		OrderReference orderRefSender = new OrderReference();
		orderRefSender.setKey("sender");
		orderRefSender.setValue(objectMapper.writeValueAsString(inquiryRequest.getSender()));
        
		request.getRequestDetail().getOrderReferences().add(orderRefSender);
		request.getRequestDetail().getOrderReferences().add(orderRef);

		log.debug("json order detail {}",objectMapper.writeValueAsString(request.getRequestDetail().getOrderReferences()));

		return ResponseHelper
                .response(String.valueOf(HttpStatus.OK.value()), "success", request.getRequestDetail(), HttpStatus.OK);
    	}
	
	public ResponseEntity getBillsPrePayment(BillPostPaymentRequest request) throws Exception {
		long startTime = new Date().getTime();
		
		SPIRequestDetail dtl = request.getData().getOrder();
		BankCashoutCommitResponse commitResponse = new BankCashoutCommitResponse();
		commitResponse.setExternalRefNumber(dtl.getExtTransactionId());
		commitResponse.setTransactionId(dtl.getOrderId());
		commitResponse.setMessageResponse(new MessageResponse());
//		commitResponse.getMessageResponse().setCode(ErrorConstant.BILLER_GENERAL_ERROR);
//		commitResponse.getMessageResponse().setDescription(dataStore.getErrorMessage(ErrorConstant.BILLER_GENERAL_ERROR));
		
		BankCashoutCommitRequest commitRequest = new BankCashoutCommitRequest();
		commitRequest.setExternalRefNumber(dtl.getExtTransactionId());
		
		Sender sender = objectMapper.readValue(dtl.getOrderReferences().stream().filter(o->o.getKey().equals("sender")).findFirst()
				.orElse(new OrderReference()).getValue(), Sender.class);
		commitRequest.setSender(sender);
		
		BankCashoutInquiryResponse bankResp = objectMapper.readValue(dtl.getOrderReferences().stream().filter(o->o.getKey().equals("bankResponse")).findFirst()
				.orElse(new OrderReference()).getValue(), BankCashoutInquiryResponse.class);
		PaymentDetails paymentDtls = bankResp.getPaymentDetails();
		
		Origination origin = new Origination();
		origin.setCountryIsoCode("IDN");
		origin.setCurrencyIsoCode("IDR");
		origin.setPrincipalAmount(dtl.getAmount().doubleValue());
		origin.setGrossAmount(dtl.getAmount().doubleValue());
		paymentDtls.setOrigination(origin);
		
		Fees fees = new Fees();
		fees.setCharges(dtl.getB().doubleValue());
		fees.setDiscount(0);
		paymentDtls.setFees(fees);
		commitRequest.setPaymentDetails(paymentDtls);

		log.debug("json request {}",objectMapper.writeValueAsString(commitRequest));
		
		commitResponse =  bankSimulator.commit(dtl.getOrderId(), commitRequest , defaultSenderAccount(), defaultCompData());
		String json = objectMapper.writeValueAsString(commitResponse);
		log.debug("json return {}",json);
//		PrePaymentResponse ppResp = new PrePaymentResponse();
//		ppResp.setData(dtl);
//		Status status = new Status();
//		status.setMessage(commitResponse.getMessageResponse().getDescription());
//		status.setCode(String.valueOf(commitResponse.getMessageResponse().getCode()));
//		ppResp.setStatus(status);
//		return new ResponseEntity(ppResp, HttpStatus.OK);
				return ResponseHelper
                .response(commitResponse.getMessageResponse().getDescription(), String.valueOf(commitResponse.getMessageResponse().getCode()), request, HttpStatus.OK);
    }
	
	
	private Account defaultSenderAccount() {
		Account acct = new Account();
		acct.setCountryCode("62");
		acct.setUsername("87889479577");
		return acct;
	}
	
	private Sender defaultSender() throws JsonMappingException, JsonProcessingException {
		String json = "{\r\n" + 
				"   \"name\":{\r\n" + 
				"      \"fullName\":\"Raja Irfan\",\r\n" + 
				"      \"firstName\":\"Raja\",\r\n" + 
				"      \"lastName\":\"Irfan\"\r\n" + 
				"   },\r\n" + 
				"   \"address\":{\r\n" + 
				"      \"address\":\"Kp Bantar Muncang Atas RT 02 RW 05, Desa Sekarwangi- Cibadak\",\r\n" + 
				"      \"city\":\"Sukabumi\",\r\n" + 
				"      \"countryIsoCode\":\"ID\",\r\n" + 
				"      \"postalCode\":\"43351\"\r\n" + 
				"   },\r\n" + 
				"   \"complianceDetails\":{\r\n" + 
				"      \"type\":\"NATIONAL_ID\",\r\n" + 
				"      \"number\":\"3202110404860010\",\r\n" + 
				"      \"issuePlace\":\"SUKABUMI\",\r\n" + 
				"      \"issueCountry\":\"ID\",\r\n" + 
				"      \"expires\":\"NE\",\r\n" + 
				"      \"documentVerified\":\"TRUE\"\r\n" + 
				"   },\r\n" + 
				"   \"mobilePhone\":{\r\n" + 
				"      \"countryCode\":\"62\",\r\n" + 
				"      \"number\":\"87889479577\"\r\n" + 
				"   },\r\n" + 
				"   \"gender\":\"M\",\r\n" + 
				"   \"dateOfBirth\":\"1986-04-04\",\r\n" + 
				"   \"placeOfBirth\":\"Jakarta\",\r\n" + 
				"   \"occupation\":\"PEKERJA SWASTA\",\r\n" + 
				"   \"sourceOfFund\":\"SALARY\",\r\n" + 
				"   \"purpose\":\"OTHER\",\r\n" + 
				"   \"relationShip\":\"FAMILY\"\r\n" + 
				"}";
		return objectMapper.readValue(json, Sender.class);
	}
	
	public ResponseEntity getProviderAmounts(GetAmountRequest request) throws Exception {
		BankCashoutInquiryRequest inquiryRequest = new BankCashoutInquiryRequest();
		inquiryRequest.setSender(defaultSender());
		inquiryRequest.setPaymentDetails(new PaymentDetails());
		inquiryRequest.getPaymentDetails().setDestination(new Destination());
		inquiryRequest.setExternalRefNumber(request.getData().getPartnerRefNumber());
		
		List<OrderReference> lst = request.getData().getBillReferences();
		inquiryRequest.getPaymentDetails().getDestination().setAccountNumber(getOrderRefVal("accountNumber", lst ));
		inquiryRequest.getPaymentDetails().getDestination().setExpectedPayoutAmount(Long.valueOf(getOrderRefVal("amount", lst)));
		inquiryRequest.getPaymentDetails().getDestination().setIssuerCode(getOrderRefVal("issuerCode", lst));
		//set payment detail 
		log.debug("json request {}",objectMapper.writeValueAsString(inquiryRequest));
		
		BankCashoutInquiryResponse response = bankSimulator.inquiry("123", inquiryRequest,defaultSenderAccount(), defaultCompData());
		request.getData().getBillReferences().add(getReference("Full Name", response.getReceiver().getName().getFullName()));
		request.getData().getBillReferences().add(getReference("Issuer Name", response.getPaymentDetails().getDestination().getIssuerName()));
		request.getData().getBillReferences().add(getReference("Expected Amount",String.valueOf(response.getPaymentDetails().getDestination().getExpectedPayoutAmount())));
		
		GetAmountResponseData result = new GetAmountResponseData();
		result.setBillReferences(request.getData().getBillReferences());
		ResponseEntity re = ResponseHelper.response("sukses", "0", result, HttpStatus.OK);
		log.debug("response >",re);
		return re;
	}
	
	private String getOrderRefVal(String key,List<OrderReference> lst) {
		OrderReference ref = new OrderReference();
		ref.setValue("notfound");
		return lst.stream().filter(f->f.getKey().equals(key)).findFirst().orElse(ref).getValue();
	}
	
	private OrderReference getReference(String key,String val) {
		OrderReference or= new OrderReference();
		or.setKey(key);
		or.setValue(val);
		return or;
	}
	
	
	private ComplianceData defaultCompData() throws JsonMappingException, JsonProcessingException {
		String json = "{\r\n" + 
				"   \"firstName\":\"Raja\",\r\n" + 
				"   \"lastName\":\"Irfan\",\r\n" + 
				"   \"address\":\"Kp Bantar Muncang Atas RT 02 RW 05, Desa Sekarwangi- Cibadak\",\r\n" + 
				"   \"city\":\"Sukabumi\",\r\n" + 
				"   \"province\":\"IDN\",\r\n" + 
				"   \"postalCode\":\"43351\",\r\n" + 
				"   \"idType\":\"NATIONAL_ID\",\r\n" + 
				"   \"idNo\":\"3202110404860010\",\r\n" + 
				"   \"issuedPlace\":\"SUKABUMI\",\r\n" + 
				"   \"country\":\"ID\",\r\n" + 
				"   \"expiredDate\":\"NE\",\r\n" + 
				"   \"countryCode\":\"62\",\r\n" + 
				"   \"gender\":\"M\",\r\n" + 
				"   \"dob\":\"1986-04-04\",\r\n" + 
				"   \"pob\":\"Jakarta\",\r\n" + 
				"   \"occupation\":\"PEKERJA SWASTA\"\r\n" + 
				"}";
		return objectMapper.readValue(json, ComplianceData.class);
		
	}
}
