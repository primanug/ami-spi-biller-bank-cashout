package id.co.truemoney.eq.spibankcashout.domain.http.request;

import java.util.HashMap;

import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Partner;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.PaymentDetails;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Receiver;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Sender;

public class BankCashoutCommitRequest {

	private Partner partner;
	private String transactionId;
	private String externalRefNumber;
	private String productId;
	private Sender sender;
	private Receiver receiver;
	private PaymentDetails paymentDetails;
	private String deviceId;
	private HashMap<String, String> extraParameters;
	
	public Sender getSender() {
		return sender;
	}
	public void setSender(Sender sender) {
		this.sender = sender;
	}
	public Partner getPartner() {
		return partner;
	}
	public void setPartner(Partner partner) {
		this.partner = partner;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getExternalRefNumber() {
		return externalRefNumber;
	}
	public void setExternalRefNumber(String externalRefNumber) {
		this.externalRefNumber = externalRefNumber;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public Receiver getReceiver() {
		return receiver;
	}
	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}
	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}
	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	public HashMap<String, String> getExtraParameters() {
		return extraParameters;
	}
	public void setExtraParameters(HashMap<String, String> extraParameters) {
		this.extraParameters = extraParameters;
	}

	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}
