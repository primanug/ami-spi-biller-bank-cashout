package id.co.truemoney.eq.spibankcashout.services.partner.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.truemoney.eq.spibankcashout.domain.http.BankSimulatorService;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Account;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Address;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.ComplianceData;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.ComplianceDetails;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Destination;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Fees;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.MobilePhone;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Name;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Origination;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.PaymentDetails;
import id.co.truemoney.eq.spibankcashout.domain.http.pojo.Sender;
import id.co.truemoney.eq.spibankcashout.domain.http.request.BankCashoutCommitRequest;
import id.co.truemoney.eq.spibankcashout.domain.http.request.BankCashoutInquiryRequest;
import id.co.truemoney.eq.spibankcashout.domain.http.response.BankCashoutCommitResponse;
import id.co.truemoney.eq.spibankcashout.domain.http.response.BankCashoutInquiryResponse;
import id.co.truemoney.eq.spibankcashout.domain.http.response.MessageResponse;
import id.co.truemoney.eq.spibankcashout.services.partner.BankSimulator;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class BankSimulatorImpl implements BankSimulator{
	
//	@Autowired
//	private DataStore dataStore;
	@Autowired
	private BankSimulatorService bankSimulatorService;
	
	@Override
	public BankCashoutInquiryResponse inquiry(String transactionId, BankCashoutInquiryRequest inquiryRequest, Account senderAccount, ComplianceData senderComplianceData) {
		BankCashoutInquiryResponse inquiryResponse = new BankCashoutInquiryResponse();
		inquiryResponse.setMessageResponse(new MessageResponse());
//		inquiryResponse.getMessageResponse().setCode(ErrorConstant.RC_SYSTEM_ERROR);
//		inquiryResponse.getMessageResponse().setDescription(dataStore.getErrorMessage(ErrorConstant.RC_SYSTEM_ERROR));
		inquiryResponse.setExternalRefNumber(inquiryRequest.getExternalRefNumber());
		
		try {
			
			BankCashoutInquiryRequest request = new BankCashoutInquiryRequest();
			request.setTransactionId(transactionId);
			request.setExternalRefNumber(inquiryRequest.getExternalRefNumber());
			
			request.setSender(new Sender());
			request.getSender().setName(new Name());
			request.getSender().getName().setFirstName(senderComplianceData.getFirstName());
			request.getSender().getName().setLastName(senderComplianceData.getLastName());
			
			request.getSender().setAddress(new Address());
			request.getSender().getAddress().setAddress(senderComplianceData.getAddress());
			request.getSender().getAddress().setCity(senderComplianceData.getCity());
			request.getSender().getAddress().setCountryIsoCode(senderComplianceData.getCountry());
			request.getSender().getAddress().setState(senderComplianceData.getProvince());
			request.getSender().getAddress().setPostalCode(senderComplianceData.getPostalCode());
			
			request.getSender().setComplianceDetails(new ComplianceDetails());
			request.getSender().getComplianceDetails().setDocumentVerified("YES");
			request.getSender().getComplianceDetails().setExpireDate(senderComplianceData.getExpiredDate());
			request.getSender().getComplianceDetails().setExpires("NE");
			request.getSender().getComplianceDetails().setIssueCountry(senderComplianceData.getCountry());
			request.getSender().getComplianceDetails().setIssuePlace(senderComplianceData.getIssuedPlace());
			request.getSender().getComplianceDetails().setNationality(senderComplianceData.getCountry());
			request.getSender().getComplianceDetails().setNumber(senderComplianceData.getIdNo());
			request.getSender().getComplianceDetails().setType(senderComplianceData.getIdType());
			
			request.getSender().setMobilePhone(new MobilePhone());
			request.getSender().getMobilePhone().setCountryCode(senderAccount.getCountryCode());
			request.getSender().getMobilePhone().setNumber(senderAccount.getUsername());
			
			request.getSender().setDateOfBirth(senderComplianceData.getDob());
			request.getSender().setGender(senderComplianceData.getGender());
			request.getSender().setOccupation(senderComplianceData.getOccupation());
			request.getSender().setPlaceOfBirth(senderComplianceData.getPob());
			request.getSender().setPurpose(inquiryRequest.getSender().getPurpose());
			request.getSender().setRelationShip(inquiryRequest.getSender().getRelationShip());
			request.getSender().setSourceOfFund(inquiryRequest.getSender().getSourceOfFund());
			
			request.setPaymentDetails(new PaymentDetails());
			request.getPaymentDetails().setDestination(new Destination());
			request.getPaymentDetails().getDestination().setAccountNumber(inquiryRequest.getPaymentDetails().getDestination().getAccountNumber());
			request.getPaymentDetails().getDestination().setIssuerCode(inquiryRequest.getPaymentDetails().getDestination().getIssuerCode());
			request.getPaymentDetails().getDestination().setExpectedPayoutAmount(inquiryRequest.getPaymentDetails().getDestination().getExpectedPayoutAmount());
//			String trxTime =  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS").format(Instant.now());
			String trxTime = getCurrentDateTime();
			request.getPaymentDetails().setTransactionTime(trxTime);
			log.debug("bank request {} ",new ObjectMapper().writeValueAsString(request));
			inquiryResponse =  bankSimulatorService.inquiry(request);
			inquiryResponse.getPaymentDetails().setTransactionTime(trxTime);
			String fullname = inquiryResponse.getReceiver().getName().getFullName();
			String[] aLastName = fullname.split("\\ ");
			String lastName = null;
			String firstName = fullname;
			if(aLastName.length > 1){
				firstName = aLastName[0];
				lastName = aLastName[aLastName.length - 1];
			}
			inquiryResponse.getReceiver().getName().setFirstName(firstName);
			inquiryResponse.getReceiver().getName().setLastName(lastName == null ? fullname : lastName);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("EXTERNAL_REF_NO[" + inquiryRequest.getExternalRefNumber() + "]"+ ExceptionUtils.getStackTrace(e));
		}
		return inquiryResponse;
	}

	@Override
	public BankCashoutCommitResponse commit(String transactionId, BankCashoutCommitRequest bankCashoutCommitRequest, 
			Account senderAccount,ComplianceData senderComplianceData) {
		// TODO Auto-generated method stub
		BankCashoutCommitResponse bankCashoutCommitResponse = new BankCashoutCommitResponse();
		bankCashoutCommitResponse.setMessageResponse(new MessageResponse());
		bankCashoutCommitResponse.setTransactionId(transactionId);
		bankCashoutCommitResponse.setExternalRefNumber(bankCashoutCommitRequest.getExternalRefNumber());
//		bankCashoutCommitResponse.getMessageResponse().setCode(ErrorConstant.RC_TRANSACTION_PENDING);
//		bankCashoutCommitResponse.getMessageResponse().setDescription(dataStore.getErrorMessage(ErrorConstant.RC_TRANSACTION_PENDING));
		
		try {
			
			BankCashoutCommitRequest commitRequest = new BankCashoutCommitRequest();
			commitRequest.setTransactionId(transactionId);
			commitRequest.setExternalRefNumber(bankCashoutCommitRequest.getExternalRefNumber());
			
			commitRequest.setSender(new Sender());
			commitRequest.getSender().setName(new Name());
			commitRequest.getSender().getName().setFirstName(senderComplianceData.getFirstName());
			commitRequest.getSender().getName().setLastName(senderComplianceData.getLastName());
			commitRequest.getSender().getName().setFullName(senderComplianceData.getFirstName()+ " "+ senderComplianceData.getLastName());
			
			commitRequest.getSender().setAddress(new Address());
			commitRequest.getSender().getAddress().setAddress(senderComplianceData.getAddress());
			commitRequest.getSender().getAddress().setCity(senderComplianceData.getCity());
			commitRequest.getSender().getAddress().setCountryIsoCode(senderComplianceData.getCountry());
			commitRequest.getSender().getAddress().setState(senderComplianceData.getProvince());
			commitRequest.getSender().getAddress().setPostalCode(senderComplianceData.getPostalCode());
			
			commitRequest.getSender().setComplianceDetails(new ComplianceDetails());
			commitRequest.getSender().getComplianceDetails().setDocumentVerified("YES");
			commitRequest.getSender().getComplianceDetails().setExpireDate(senderComplianceData.getExpiredDate());
			commitRequest.getSender().getComplianceDetails().setExpires("NE");
			commitRequest.getSender().getComplianceDetails().setIssueCountry(senderComplianceData.getCountry());
			commitRequest.getSender().getComplianceDetails().setIssuePlace(senderComplianceData.getIssuedPlace());
			commitRequest.getSender().getComplianceDetails().setNationality(senderComplianceData.getCountry());
			commitRequest.getSender().getComplianceDetails().setNumber(senderComplianceData.getIdNo());
			commitRequest.getSender().getComplianceDetails().setType(senderComplianceData.getIdType());
			
			commitRequest.getSender().setMobilePhone(new MobilePhone());
			commitRequest.getSender().getMobilePhone().setCountryCode(senderAccount.getCountryCode());
			commitRequest.getSender().getMobilePhone().setNumber(senderAccount.getUsername());
			
			commitRequest.getSender().setDateOfBirth(senderComplianceData.getDob());
			commitRequest.getSender().setGender(senderComplianceData.getGender());
			commitRequest.getSender().setOccupation(senderComplianceData.getOccupation());
			commitRequest.getSender().setPlaceOfBirth(senderComplianceData.getPob());
			commitRequest.getSender().setPurpose(bankCashoutCommitRequest.getSender().getPurpose());
			commitRequest.getSender().setRelationShip(bankCashoutCommitRequest.getSender().getRelationShip());
			commitRequest.getSender().setSourceOfFund(bankCashoutCommitRequest.getSender().getSourceOfFund());
			
			commitRequest.setPaymentDetails(new PaymentDetails());
			
			commitRequest.getPaymentDetails().setOrigination(new Origination());
			commitRequest.getPaymentDetails().getOrigination().setPrincipalAmount(bankCashoutCommitRequest.getPaymentDetails().getOrigination().getPrincipalAmount());
			commitRequest.getPaymentDetails().getOrigination().setGrossAmount(bankCashoutCommitRequest.getPaymentDetails().getOrigination().getGrossAmount());
			commitRequest.getPaymentDetails().getOrigination().setCurrencyIsoCode(bankCashoutCommitRequest.getPaymentDetails().getOrigination().getCurrencyIsoCode());
			commitRequest.getPaymentDetails().getOrigination().setCountryIsoCode(bankCashoutCommitRequest.getPaymentDetails().getOrigination().getCountryIsoCode());
			
			commitRequest.getPaymentDetails().setDestination(new Destination());
			commitRequest.getPaymentDetails().getDestination().setAccountNumber(bankCashoutCommitRequest.getPaymentDetails().getDestination().getAccountNumber());
			commitRequest.getPaymentDetails().getDestination().setIssuerCode(bankCashoutCommitRequest.getPaymentDetails().getDestination().getIssuerCode());
			commitRequest.getPaymentDetails().getDestination().setExpectedPayoutAmount(bankCashoutCommitRequest.getPaymentDetails().getDestination().getExpectedPayoutAmount());
			commitRequest.getPaymentDetails().setTransactionTime(getCurrentDateTime());
			
			commitRequest.getPaymentDetails().setFees(new Fees());
			commitRequest.getPaymentDetails().getFees().setCharges(bankCashoutCommitRequest.getPaymentDetails().getFees().getCharges());
			commitRequest.getPaymentDetails().getFees().setDiscount(bankCashoutCommitRequest.getPaymentDetails().getFees().getDiscount());
			
			bankCashoutCommitResponse = bankSimulatorService.commit(commitRequest);
			
		}catch (ResourceAccessException e) {
			// TODO: handle exception
			log.error("EXTERNAL_REF_NO[" + bankCashoutCommitRequest.getExternalRefNumber() + "]"+ ExceptionUtils.getStackTrace(e));
		} catch (Exception e) {
			// TODO: handle exception
			log.error("EXTERNAL_REF_NO[" + bankCashoutCommitRequest.getExternalRefNumber() + "]"+ ExceptionUtils.getStackTrace(e));
		}
		
		return bankCashoutCommitResponse;
	}
	
	public static String getCurrentDateTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String formDate = "";
		Date newDate = new Date();
		formDate = df.format(newDate);
		return formDate;
	}
}
