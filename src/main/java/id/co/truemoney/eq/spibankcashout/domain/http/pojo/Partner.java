/**
 * Project    : International Remittance - Single API 
 * Created By : Megi Permana
 * Date       : Jul 24, 2018
 * Time       : 12:30:10 PM 
 */
package id.co.truemoney.eq.spibankcashout.domain.http.pojo;

public class Partner {

	private String id;
	private System system;
	private Credential credential;
	
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}



	/**
	 * @return the system
	 */
	public System getSystem() {
		return system;
	}



	/**
	 * @param system the system to set
	 */
	public void setSystem(System system) {
		this.system = system;
	}



	/**
	 * @return the credential
	 */
	public Credential getCredential() {
		return credential;
	}



	/**
	 * @param credential the credential to set
	 */
	public void setCredential(Credential credential) {
		this.credential = credential;
	}



	@Override
	public String toString() {
		return "Partner [id=" + id + ", system=" + system + ", credential=" + credential + "]";
	}
	
	
	
}
