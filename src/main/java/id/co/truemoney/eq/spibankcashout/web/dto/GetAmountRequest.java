package id.co.truemoney.eq.spibankcashout.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.ToString;

@JsonIgnoreProperties(
        ignoreUnknown = true
)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
@ToString(callSuper = true)
public class GetAmountRequest extends BaseEquatorRequest<GetAmountRequestData> {
    public GetAmountRequest() {
    }
}
