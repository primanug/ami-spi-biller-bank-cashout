apiVersion: v1
kind: BuildConfig
metadata:
  name: "spi-biller-prepare-promote-to-production"
  namespace: '#NAMESPACE#'
  labels:
    envName: 'production'
    appName: 'spi-biller'
    appVersion: '#APP_VERSION#'
    appScope: 'truemoney'
    appLang: 'springboot'
    svcGroup: 'common'
    pipelineVersion: '1.8.1-release'
spec:
  strategy:
    jenkinsPipelineStrategy:
      jenkinsfile: |-
        #!/usr/bin/groovy
        @Library('pipeline_library@1.8.1-release')
        import groovy.json.*
        import java.text.SimpleDateFormat
        import groovy.json.JsonSlurperClassic

        // Global Variable Declartion
        def ENV_NAME = "production"
        def NAMESPACE_CICD = ""
        def NAMESPACE_PRODUCTION = ""
        def OPENSHIFTVERSION = ""
        def GLOBAL_VARS = ""
        def BUILD_INFO = ""
        def APP_NAME = ""
        def APP_VERSION = ""
        def APP_URL_TYPE_SERVICE = ""
        def STAGE_CURRENT = ""
        def COUNTRY_CODE = ""
        def APP_SCOPE = ""
        def RELEASE_TRANS_ID = "#RELEASE_TRANSID#"
        def STAGE_STATUS = ""

        properties([
          parameters([
            string(name: 'app_name', defaultValue: 'spi-biller', description: 'Applcation name'),
            string(name: 'app_version', defaultValue: '#APP_VERSION#', description: 'Applcation version')
          ]), 
          [
            $class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '10']
          ]
        ])

        try {
          APP_NAME = app_name
        } catch (Throwable e) {
          APP_NAME = "spi-biller"
        }

        try {
          APP_VERSION = app_version
        } catch (Throwable e) {
          APP_VERSION = "#APP_VERSION#"
        }

        deploymentNode() {

          def directory_workspace = pwd()

          container(name: 'jnlp'){

            OPENSHIFTVERSION = acnGetVersionOpenshift()
            sh "oc label pod ${HOSTNAME} appScope=openshift-cicd appName=openshift-cicd"
            COUNTRY_CODE = sh script: "cat /country-code/countrycode.txt", returnStdout: true
            COUNTRY_CODE = COUNTRY_CODE.trim()
            NAMESPACE = sh script: "cat /var/run/secrets/kubernetes.io/serviceaccount/namespace", returnStdout: true
            NAMESPACE = NAMESPACE.trim()
            APP_SCOPE = NAMESPACE.substring(0, NAMESPACE.indexOf('-'))

            try {

              try {
                acnSendForCreateTimeRecording{
                  releaseTransId = RELEASE_TRANS_ID
                  stageName = "sync-artifact"
                  stageAction = "start"
                }
              } catch(e) {
                echo "ignore"
              }

              dir("${directory_workspace}/s3-pull-artifact"){

                withAWS(credentials:'openshift-s3-credential', endpointUrl: 'https://s3-ap-southeast-1.amazonaws.com', region: 'ap-southeast-1') {
                  s3Download pathStyleAccessEnabled: true, bucket: 'openshift-distributed-artifacts', file: "${APP_NAME}-${APP_VERSION}.tar.gz", path: "OPENSHIFT/${COUNTRY_CODE}/${APP_SCOPE}/${APP_NAME}/${APP_NAME}-${APP_VERSION}.tar.gz"
                }
                sh "/bin/tar -zxvf ${APP_NAME}-${APP_VERSION}.tar.gz -C ${directory_workspace}/s3-pull-artifact"
                
              }

              script {
                GLOBAL_VARS = readProperties  file:"${directory_workspace}/s3-pull-artifact/${APP_NAME}-${APP_VERSION}/pipeline/variables.properties"
              }

              stage('Sync Configuration Server') {

                STAGE_CURRENT = "Sync-Configuration-Server"

                dir("${directory_workspace}/s3-pull-config") {

                  acnSyncConfiguration_Status = acnSyncConfigurationServer {
                    global_vars = GLOBAL_VARS
                    list_env = ["production"]
                    version = APP_VERSION
                    country_code = COUNTRY_CODE
                    config_path = "/app-configs"
                    directory = directory_workspace
                  }

                }

              }

              stage('Pull Image From GCR'){

                STAGE_CURRENT = "Pull-image-from-GCR"

                acnPrepareBuildConfigForPullImageFromGCR {
                  appName = APP_NAME
                  appVersion = APP_VERSION
                  appScope = APP_SCOPE
                  country_code = COUNTRY_CODE
                  openshift_version = OPENSHIFTVERSION
                  envName = ENV_NAME
                  directory = directory_workspace
                }

                NAMESPACE_CICD = APP_SCOPE + "-cicd"

                def localBCFile = "${directory_workspace}/s3-pull-artifact/${APP_NAME}-${APP_VERSION}/pipeline/openshift-artifacts/${OPENSHIFTVERSION}/buildconfigs/pull-image-buildconfig.yaml"

                // # GitOps : Checkout
                gitOps.checkout(directory_workspace, APP_SCOPE, ENV_NAME, APP_NAME, APP_VERSION, GLOBAL_VARS['GITOPS_REPO_NAME'])
                // # GitOps : Add artifact file 'pull-image-buildconfig.yaml'
                gitOps.addFile(directory_workspace, APP_SCOPE, ENV_NAME, APP_NAME, APP_VERSION, GLOBAL_VARS['GITOPS_REPO_NAME'], localBCFile, "BC", "pull-image-buildconfig.yaml")
                // # GitOps : Push to repository
                gitOps.push(directory_workspace, APP_SCOPE, ENV_NAME, APP_NAME, APP_VERSION, GLOBAL_VARS['GITOPS_REPO_NAME'])

                sh "oc apply -f ${localBCFile} -n ${NAMESPACE_CICD}"
                sh "oc start-build ${APP_NAME}-pull-images-buildconfig --follow --wait -n ${NAMESPACE_CICD}"
                
              }

              STAGE_STATUS = "success"

            } catch(e) {

              STAGE_STATUS = "fail"

            } finally {

              try {
                acnSendForCreateTimeRecording{
                  releaseTransId = RELEASE_TRANS_ID
                  stageName = "sync-artifact"
                  stageAction = "end-with-${STAGE_STATUS}"
                }
              } catch(e) {
                echo "ignore"
              }

              if ( STAGE_STATUS == "fail" ) {
                currentBuild.result = "FAILURE"
                error "Pipeline failure stage: ${STAGE_CURRENT}"
              }
              
            }

          } // End Container JNLP

        } // End Deploy Node
    type: JenkinsPipeline