apiVersion: v1
kind: BuildConfig
metadata:
  name: "spi-biller-promote-to-staging"
  namespace: '#NAMESPACE#'
  labels:
    envName: 'staging'
    appName: 'spi-biller'
    appVersion: '#APP_VERSION#'
    appScope: 'truemoney'
    appLang: 'springboot'
    svcGroup: 'common'
    pipelineVersion: '1.8.1-release'
spec:
  strategy:
    jenkinsPipelineStrategy:
      jenkinsfile: |-
        #!/usr/bin/groovy
        @Library('pipeline_library@1.8.1-release')
        import groovy.json.*
        import java.text.SimpleDateFormat
        import groovy.json.JsonSlurperClassic

        // Global Variable Declartion
        def ENV_NAME = "staging"
        def NAMESPACE_CICD = ""
        def NAMESPACE_STAGING = ""
        def OPENSHIFT_VERSION = ""
        def GLOBAL_VARS = ""
        def BUILD_INFO = ""
        def APP_NAME = ""
        def APP_VERSION = ""
        def APP_URL_TYPE_SERVICE = ""
        def STAGE_CURRENT = ""
        def COUNTRY_CODE = ""
        def BUILD_INFO_LIST = []
        def APP_SCOPE = ""
        def DB_DEPLOYMENT_ENABLE = "#DB_DEPLOYMENT_ENABLE#"
        def DEPLOYMENT_TYPE = "#DEPLOYMENT_TYPE#"
        def RELEASE_TRANS_ID = "#RELEASE_TRANSID#"
        def IS_DISRUPTIVE = "#IS_DISRUPTIVE#"
        def STAGE_CATCH = ""

        properties([
          parameters([
            string(name: 'app_name', defaultValue: 'spi-biller', description: 'Applcation name'),
            string(name: 'app_version', defaultValue: '#APP_VERSION#', description: 'Applcation version')
          ]), 
          [
            $class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '10']
          ]
        ])

        try {
          APP_NAME = app_name
        } catch (Throwable e) {
          APP_NAME = "spi-biller"
        }

        try {
          APP_VERSION = app_version
        } catch (Throwable e) {
          APP_VERSION = "#APP_VERSION#"
        }

        deploymentNode() {

          def directory_workspace = pwd()

          container(name: 'jnlp'){

            OPENSHIFT_VERSION = acnGetVersionOpenshift()
            sh "oc label pod ${HOSTNAME} appScope=openshift-cicd appName=openshift-cicd"
            COUNTRY_CODE = sh script: "cat /country-code/countrycode.txt", returnStdout: true
            COUNTRY_CODE = COUNTRY_CODE.trim()
            NAMESPACE = sh script: "cat /var/run/secrets/kubernetes.io/serviceaccount/namespace", returnStdout: true
            NAMESPACE = NAMESPACE.trim()
            APP_SCOPE = NAMESPACE.substring(0, NAMESPACE.indexOf('-'))

            try {
              try {
                acnSendForCreateTimeRecording{
                  releaseTransId = RELEASE_TRANS_ID
                  stageName = "sync-artifact"
                  stageAction = "skip"
                }
              } catch(e) {
                echo "ignore"
              }
              
              stage('Download Artifact From S3'){
                STAGE_CURRENT = "Download-Artifact-From-S3"
                withAWS(credentials:'openshift-s3-credential', endpointUrl: 'https://s3-ap-southeast-1.amazonaws.com', region: 'ap-southeast-1') {
                  s3Download pathStyleAccessEnabled: true, bucket: 'openshift-distributed-artifacts', file: "${APP_NAME}-${APP_VERSION}.tar.gz", path: "OPENSHIFT/${COUNTRY_CODE}/${APP_SCOPE}/${APP_NAME}/${APP_NAME}-${APP_VERSION}.tar.gz"
                }
                sh "/bin/tar -zxvf ${APP_NAME}-${APP_VERSION}.tar.gz -C ${directory_workspace}"
                sh "cp -rf ${directory_workspace}/${APP_NAME}-${APP_VERSION}/* ${directory_workspace}/"
                script {
                  GLOBAL_VARS = readProperties  file:"pipeline/variables.properties"
                  BUILD_INFO = readProperties  file:"build_info.properties"
                  NAMESPACE_CICD = "${GLOBAL_VARS['APP_SCOPE']}-cicd"
                  NAMESPACE_STAGING = "${GLOBAL_VARS['APP_SCOPE']}-${GLOBAL_VARS['APP_SVC_GROUP']}-staging"
                }
                BUILD_INFO_LIST.add("none") // 0 GIT_TAG
                BUILD_INFO_LIST.add("none") // 1 GIT_AUTHOR
                BUILD_INFO_LIST.add(BUILD_INFO['GIT_HASH_APPLICATION']) // 2 GIT_HASH
                BUILD_INFO_LIST.add(BUILD_INFO['RERUN_CONDITION_ACTION']) // 3 RERUN_CONDITION_ACTION
                BUILD_INFO_LIST.add("none") // 4 BUILD_ARTIFACTS_ONLY_AND_SKIP_OPENSHIFT_DEPLOY
                BUILD_INFO_LIST.add(BUILD_INFO['BUILD_ON_BRANCH']) // 5 BUILD_ON_BRANCH
                BUILD_INFO_LIST.add(BUILD_INFO['FORCE_DEPLOY_NETWORK_POLICY']) // 6 FORCE_DEPLOY_NETWORK_POLICY
                BUILD_INFO_LIST.add(BUILD_INFO['FORCE_DEPLOY_AUTOSCALING']) // 7 FORCE_DEPLOY_AUTOSCALING
                BUILD_INFO_LIST.add(BUILD_INFO['FORCE_DEPLOY_ROUTE']) // 8 FORCE_DEPLOY_ROUTE
                BUILD_INFO_LIST.add(BUILD_INFO['FORCE_DEPLOY_NETWORK_POLICY_FIRST_BUILD']) // 9 FORCE_DEPLOY_NETWORK_POLICY_FIRST_BUILD
                BUILD_INFO_LIST.add(BUILD_INFO['FORCE_DEPLOY_AUTOSCALING_FIRST_BUILD']) // 10 FORCE_DEPLOY_AUTOSCALING_FIRST_BUILD
                BUILD_INFO_LIST.add(BUILD_INFO['FORCE_DEPLOY_ROUTE_FIRST_BUILD']) // 11 FORCE_DEPLOY_ROUTE_FIRST_BUILD
                BUILD_INFO_LIST.add(GLOBAL_VARS['DATABASE_DEPLOY']) // 12 DATABASE_DEPLOY
              }

              stage('Deployment Resources For STAGING Environment'){
                STAGE_CURRENT = "Deployment-database-or-application"
                def rcStaging = acnGetDeploymentResources {
                  global_vars = GLOBAL_VARS
                  appName = GLOBAL_VARS['APP_NAME']
                  versionOpenshift = OPENSHIFT_VERSION
                  appVersion = APP_VERSION
                  envName = ENV_NAME
                  replicaNum = GLOBAL_VARS['DEFAULT_NUM_REPLICA_STAGING']
                  routeHostname = GLOBAL_VARS['ROUTE_HOSTNAME_STAGING']
                  routerSharding = GLOBAL_VARS['ROUTE_SHARDING']
                  namespace_env = NAMESPACE_STAGING
                  gitHashApplication = BUILD_INFO['GIT_HASH_APPLICATION']
                  gitSourceBranch = BUILD_INFO['BUILD_ON_BRANCH']
                  forceDeployList = BUILD_INFO_LIST
                  directoryWorkspace = directory_workspace
                  country_code = COUNTRY_CODE
                  deploymentType = DEPLOYMENT_TYPE
                  dbDeploymentEnable = DB_DEPLOYMENT_ENABLE
                  releaseTransId = RELEASE_TRANS_ID
                  config_path = "/app-configs"
                  deploy_mountebank_enable = "no_action_type_in_staging"
                  gitOpsRepoName = GLOBAL_VARS['GITOPS_REPO_NAME']
                }
              } // End Stage deploy to staging

              stage('Verify version application has changed'){
                STAGE_CURRENT = "deploy-application"
                APP_URL_TYPE_SERVICE = new URL("${GLOBAL_VARS['APP_PROTOCOL']}://${GLOBAL_VARS['APP_NAME']}.${NAMESPACE_STAGING}.svc:${GLOBAL_VARS['APP_PORT']}${GLOBAL_VARS['PATH_INFO']}")
                acnVerifyVersion {
                  global_vars = GLOBAL_VARS
                  app_url_openshift_format = APP_URL_TYPE_SERVICE
                  version = APP_VERSION
                  isDisruptive = IS_DISRUPTIVE
                }
              } // End Stage Verify version

              STAGE_CATCH = "success"
              try {
                acnSendForCreateTimeRecording{
                  releaseTransId = RELEASE_TRANS_ID
                  stageName = "deploy-application"
                  stageAction = "end-with-success"
                }
                acnSendForCreateTimeRecording{
                  releaseTransId = RELEASE_TRANS_ID
                  stageName = "run-sanity-test"
                  stageAction = "skip"
                }
              } catch(e) {
                echo "ignore"
              }
            } catch(e) {
              if ( e.toString().contains("deploy database") || STAGE_CURRENT == "Download-Artifact-From-S3" ) {
                error "FAIL"
              } else {
                acnSendForCreateTimeRecording{
                  releaseTransId = RELEASE_TRANS_ID
                  stageName = "deploy-application"
                  stageAction = "end-with-fail"
                } 
                currentBuild.result = "FAILURE"
                error "deploy fail"
              }
            } // try catch
          } // End Container JNLP
        } // End Deploy Node
    type: JenkinsPipeline