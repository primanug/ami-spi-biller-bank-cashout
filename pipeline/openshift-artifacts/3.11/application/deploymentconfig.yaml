apiVersion: v1
kind: DeploymentConfig
metadata:
  annotations:
    template.alpha.openshift.io/wait-for-ready: 'false'
    pathInfo: '/spi-biller//info'
    appProtocal: 'https'
    appPort: '8443'
    appMetricsPort: '18443'
  labels:
    envName: '#ENV_NAME#'
    appName: 'spi-biller'
    appVersion: '#APP_VERSION#'
    appScope: 'truemoney'
    appLang: 'springboot'
    svcGroup: 'common'
    pipelineVersion: '1.8.1-release'
    buildHash: '#BUILD_HASH#'
    buildFromBranch: '#GIT_SOURCE_BRANCH#'
    countryCode: '#COUNTRY_CODE#'
    deployStretagy: 'RollingUpdate'
    metricsCollector: 'prometheus-cicd-01'
    logType: 'application'
  name: 'spi-biller'
  namespace: '#NAMESPACE#'
spec:
  replicas: '#NUM_OF_REPLICA#'
  selector:
    envName: '#ENV_NAME#'
    appName: 'spi-biller'
  strategy:
    rollingParams:
      intervalSeconds: 1
      maxSurge: '25%'
      maxUnavailable: '0%'
      timeoutSeconds: 600
      updatePeriodSeconds: 1
    type: Rolling
  template:
    metadata:
      annotations:
      labels:
        envName: '#ENV_NAME#'
        appName: 'spi-biller'
        appVersion: '#APP_VERSION#'
        appScope: 'truemoney'
        appLang: 'springboot'
        svcGroup: 'common'
        pipelineVersion: '1.8.1-release'
        buildHash: '#BUILD_HASH#'
        buildFromBranch: '#GIT_SOURCE_BRANCH#'
        countryCode: '#COUNTRY_CODE#'
        metricsCollector: 'prometheus-cicd-01'
        deployStretagy: 'RollingUpdate'
        logType: 'application'
    spec:
      containers:
      - env:
        - name: KUBERNETES_NAMESPACE
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
        - name: ENV_NAME
          value: '#ENV_NAME#'
        - name: VAULT_SECRET_API_VERSION
          valueFrom:
            configMapKeyRef:
              name: vault-secret-api-version
              key: version
        - name: TZ
          value: 'Asia/Jakarta'
        - name: APP_LANG
          value: 'springboot'
        - name: COUNTRY_CODE
          value: '#COUNTRY_CODE#'
        - name: RUNWAY_NAME
          value: 'OPENSHIFT'
        - name: APP_NAME
          value: 'spi-biller'
        - name: APP_SCOPE
          value: 'truemoney'
        - name: APP_STARTUP_ARGS
          value: '-Djdk.tls.useExtendedMasterSecret=false -Xms256m -Xmx1024m'
        - name: APP_SERVICE_GROUP
          value: 'common'
        image: '#IMAGE_URL#'
        imagePullPolicy: Always
        affinity:
          podAntiAffinity: 
            preferredDuringSchedulingIgnoredDuringExecution: 
            - weight: 100  
              podAffinityTerm:
                labelSelector:
                  matchLabels:
                    deployment-nodegroup: '#ENV_NAME#'
                topologyKey: kubernetes.io/hostname
        name: 'spi-biller'
        ports:
        - containerPort: 8443
          name: 'https'
          protocol: TCP
        - containerPort: 18443
          name: 'metrics'
          protocol: TCP
        livenessProbe:
            failureThreshold: 3
            httpGet:
              path: '/spi-biller//health'
              port: 8443
              scheme: 'HTTPS'
            initialDelaySeconds: 60
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 5
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: '/spi-biller//health'
            port: 8443
            scheme: 'HTTPS'
          initialDelaySeconds: 60
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 5
        lifecycle:
          postStart:
            exec:
              command: ['sh', '-c', 'sleep 0s']
          preStop:
            exec:
              command: ['sh', '-c', 'sleep 10s']
        resources:
          limits:
            cpu: '400m'
            memory: '1024Mi'
          requests:
            cpu: '200m'
            memory: '256Mi'
        securityContext:
          supplementalGroups: [65534]
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /var/run/secrets/java.io/keystores
          name: keystore-volume
        - mountPath: /var/run/secrets/openshift.io/services_serving_certs
          name: spi-biller-service-certs
      initContainers:
      - name: pem-to-keystore
        image: '#DOCKER_REGISTRY_SERVICE_IP#:5000/truemoney-cicd/sso71-openshift:1.1-16'
        imagePullPolicy: Always
        env:
        - name: keyfile
          value: /var/run/secrets/openshift.io/services_serving_certs/tls.key
        - name: crtfile
          value: /var/run/secrets/openshift.io/services_serving_certs/tls.crt
        - name: keystore_pkcs12
          value: /var/run/secrets/java.io/keystores/keystore.pkcs12
        - name: keystore_jks
          value: /var/run/secrets/java.io/keystores/keystore.jks
        - name: password
          value: changeit
        - name: ca_bundle
          value: /var/run/secrets/kubernetes.io/serviceaccount/service-ca.crt
        - name: truststore_jks
          value: /var/run/secrets/java.io/keystores/truststore.jks
        command: ['/bin/bash']
        args: ['-c', "openssl pkcs12 -export -inkey $keyfile -in $crtfile -out $keystore_pkcs12 -password pass:$password && keytool -importkeystore -noprompt -srckeystore $keystore_pkcs12 -srcstoretype pkcs12 -destkeystore $keystore_jks -storepass $password -srcstorepass $password; keytool -importkeystore -srckeystore $JAVA_HOME/jre/lib/security/cacerts -srcstoretype JKS -destkeystore $truststore_jks -storepass changeit -srcstorepass changeit && csplit -z -f crt- $ca_bundle '/-----BEGIN CERTIFICATE-----/' '{*}' && for file in crt-*; do keytool -import -noprompt -keystore $truststore_jks -file $file -storepass changeit -alias service-$file; done"]
        volumeMounts:
        - mountPath: /var/run/secrets/java.io/keystores
          name: keystore-volume
        - mountPath: /var/run/secrets/openshift.io/services_serving_certs
          name: spi-biller-service-certs
        resources:
          limits:
            cpu: 500m
            memory: 256Mi
          requests:
            cpu: 100m
            memory: 256Mi
      volumes:
      - name: keystore-volume
        emptyDir: {}
      - name: 'spi-biller-service-certs'
        secret:
          secretName: 'spi-biller-service-certs'
      dnsPolicy: ClusterFirst
      nodeSelector:
        deployment-nodegroup: '#ENV_NAME#'
      restartPolicy: Always
      schedulerName: default-scheduler
      serviceAccount: acm-deployer
      serviceAccountName: acm-deployer
      securityContext: 
        supplementalGroups: [65534]
      terminationGracePeriodSeconds: 65
  triggers:
  - type: ConfigChange