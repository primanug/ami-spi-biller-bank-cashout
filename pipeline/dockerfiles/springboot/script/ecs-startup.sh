#!/bin/bash

# Add Docker Host Info
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
echo $env_name
echo $app_name
echo $country_code
echo $java_xmx
echo $java_xms

app_version=$(cat /image_info/app_version)
git_revision=$(cat /image_info/git_revision)
config_url="https://s3-ap-southeast-1.amazonaws.com/acm-aws-configuration-repo"
vault_file="/data/projects/$app_name/config/vault/secret.sh"

echo "Pull Configuration"
cd /tmp
wget $config_url/$country_code/$env_name/$app_name/$app_name-$app_version.tar.gz
tar -zxvf $app_name-$app_version.tar.gz -C . && mv /tmp/$app_name-$app_version/* /data/projects/$app_name/config/

echo "Modify Credential"
if [ -f "$vault_file" ];
then
   chmod +x $vault_file
   $vault_file
fi

echo "Start JMX Expoter"
if [ "$MONITOR_ENABLE" == "true" ];
then
  cd /data/projects/$app_name/config
  /usr/bin/java -Xms64m -Xmx128m -jar /tmp/jmx_prometheus_httpserver-0.7-SNAPSHOT-jar-with-dependencies.jar 5556 config.yml &
fi

echo "Start Application"
chmod +x /data/projects/$app_name/$app_name-$app_version.#PACKAGE_EXTENSION#
cd /data/projects/$app_name
java -Xms$java_xms -Xmx$java_xmx -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=1099 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -jar /data/projects/$app_name/$app_name-$app_version.#PACKAGE_EXTENSION#