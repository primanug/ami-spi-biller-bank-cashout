#!/bin/bash

#Random UID case handling
if ! whoami &> /dev/null; then
  if [ -w /etc/passwd ]; then
    echo "${USER_NAME:-default}:x:$(id -u):0:${USER_NAME:-default} user:${HOME}:/sbin/nologin" >> /etc/passwd
  fi
fi

mb --pidfile /tmp/mb.pid --logfile /data/logs/mountebank/mb.log --allowInjection
