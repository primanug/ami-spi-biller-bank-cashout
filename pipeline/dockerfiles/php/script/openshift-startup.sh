#!/bin/bash
#####################################################################################################
#  Pipeline Generator for Ascend's application                                                      #
#  Create by devops team                                                                            #
#  Ascend Group Co., Ltd.                                                                           #
#####################################################################################################

# Add Docker Host Info
export LANG="en_US.UTF-8"

echo "Current user is " $(whoami)
echo "Current id is " $(id)
APP_NAME=$(cat /image_info/app_name)
APP_VERSION=$(cat /image_info/app_version)
#COUNTRY_CODE=$(cat /image_info/country_code)
APP_NAME=$(cat /image_info/app_name)
APP_VERSION=$(cat /image_info/app_version)
COUNTRY_CODE=$(cat /image_info/country_code)
APP_PROTOCOL=$(cat /image_info/app_protocol)
APP_PORT=$(cat /image_info/app_port)
NAMESPACE=$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace)
DATE_FORMAT="+%Y-%m-%d %H:%M:%S"

echo "Current environment is " $ENV_NAME
echo "Current appscope is " $APP_SCOPE
echo "Current country code is " $COUNTRY_CODE
echo "Application protocol is " $APP_PROTOCOL
echo "Application port is " $APP_PORT

config_url="http://configuration-server.$APP_SCOPE-cicd.svc/configs"
vault_file="/data/projects/$APP_NAME/config/vault/secret.sh"
echo "not-ready" > /tmp/checkpoint_vault.txt

echo "$(date "${DATE_FORMAT}") | Pulling Configuration"
cd /tmp
HTTP_RESPONSE=$(curl --silent --write-out "%{http_code}" -kO $config_url/$ENV_NAME/$APP_NAME/$APP_NAME-$APP_VERSION.tar.gz)
echo "$(date "${DATE_FORMAT}") | Http status when pulling configuration is ${HTTP_RESPONSE}"
if [[ ${HTTP_RESPONSE} != 200 ]]; then
  echo "$(date "${DATE_FORMAT}") | Failed : Cannot pull configure"
  sleep 2s
  exit 1
else
  echo "$(date "${DATE_FORMAT}") | Pulled Configuration"
  echo "$(date "${DATE_FORMAT}") | Extract Configuration file"
  tar -zxvf $APP_NAME-$APP_VERSION.tar.gz -C . && mv /tmp/$APP_NAME-$APP_VERSION/* /data/projects/$APP_NAME/config/
  echo "$(date "${DATE_FORMAT}") | Extracted Configuration file"
  echo "$(date "${DATE_FORMAT}") | Check script file for get sensitive data from vault in folder config."
  if [ -f "$vault_file" ];
  then
    echo "$(date "${DATE_FORMAT}") | Start get sensitive data from vault server."
    chmod +x $vault_file
    $vault_file
    CHECKPOINT_VAULT=$(cat /tmp/checkpoint_vault.txt)
    if [ "$CHECKPOINT_VAULT" == "ready-to-start" ]
    then
      echo "$(date "${DATE_FORMAT}") | Finish get sensitive data from vault server."
    else
      echo "$(date "${DATE_FORMAT}") | Failed : Cannot get sensitive data from vault server."
      sleep 2s
      exit 1
    fi
  else
    echo "$(date "${DATE_FORMAT}") | ${APP_NAME} don't have script for use vault server."
  fi
  echo "$(date "${DATE_FORMAT}") | Copy configure"
  cp /data/projects/$APP_NAME/config/env /usr/share/nginx/html/$APP_NAME/.env
  echo "$(date "${DATE_FORMAT}") | Start fast-cgi"
  exec php-cgi -q -b 127.0.0.1:9000 &>/dev/null &
  echo "$(date "${DATE_FORMAT}") | Generate Key"
  cd /usr/share/nginx/html/$APP_NAME 
  exec php artisan key:generate &>/dev/null &
  sleep 4s
  echo "$(date "${DATE_FORMAT}") | Start nginx-vts-exporter."
  exec /bin/nginx-vts-exporter -nginx.scrape_uri=$APP_PROTOCOL://localhost:$APP_PORT/status/format/json --telemetry.address :18443 -telemetry.endpoint /prometheus &>/dev/null &
  echo "$(date "${DATE_FORMAT}") | Start nginx."
  exec nginx -c /data/projects/$APP_NAME/config/nginx.conf -g "daemon off;"
fi