#!/bin/bash

#####################################################################################################
#  Pipeline Generator for Ascend's application														#
#  Create by devops team																			#
#  Ascend Group Co., Ltd.																			#
#####################################################################################################

export LANG="en_US.UTF-8"
echo $ENV
echo $APP_NAME
echo $CONFIG_PATH
echo $COUNTRY_CODE
echo $CONSUL_SERVER_LIST
echo $JAVA_XMX
echo $JAVA_XMS
host_ip=$(cat /temp/host_ip)
app_version=$(cat /image_info/app_version)
git_revision=$(cat /image_info/git_revision)
config_url="config-server.service.consul"
vault_file="/data/projects/$APP_NAME/config/secret.sh"

echo "Modify Bind9 Configuration"
sed -i "s/#consul-server-list#/$CONSUL_SERVER_LIST/g" /etc/bind/named.conf
sed -i "s/#host-ip#/$host_ip/g" /etc/bind/consul.conf

echo "Start Bind9 Service"
sudo /usr/sbin/named -f -u bind -4 > /dev/null 2>&1 &

echo "Pull Configuration"
cd /data/projects/$APP_NAME/config && wget -r -nH -np -R index.html* --cut-dirs=1 --cut-dirs=2 --cut-dirs=3  --cut-dirs=4 --cut-dirs=5 http://$config_url/configs/$ENV/$APP_NAME/$app_version/
cd /data/projects/$APP_NAME/config && wget -r -nH -np -R index.html* --cut-dirs=1 --cut-dirs=2 --cut-dirs=3  http://$config_url/configs/$ENV/platform-configs/
cd /data/projects/$APP_NAME/config && wget http://$config_url/configs/$ENV/config.yml

echo "Modify Environment variable"
property_file_list=$(find /data/projects/$APP_NAME/config -type f | xargs grep '#ENV#' | cut -f1 -d ":")
echo $property_file_list

for filename in ${property_file_list}
do
   sed -i "s/#ENV#/$ENV/g" $filename
done

echo "Modify Credential"
if [ -f "$vault_file" ];
then
   chmod +x /data/projects/$APP_NAME/config/secret.sh
   /data/projects/$APP_NAME/config/secret.sh
fi


echo "Start JMX Expoter"
if [ "$MONITOR_ENABLE" == "true" ];
then
  cd /data/projects/$APP_NAME/config
  /usr/bin/java -Xms16m -Xmx32m -jar /tmp/jmx_prometheus_httpserver-0.7-SNAPSHOT-jar-with-dependencies.jar 5556 config.yml &
fi

echo "Start Application"
chmod +x /data/projects/$APP_NAME/$APP_NAME-$app_version.#PACKAGE_EXTENSION#
java -XX:MaxMetaspaceSize=128m -Xss1m -Xms$JAVA_XMS -Xmx$JAVA_XMX -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=1099 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djavax.net.ssl.trustStore=/data/projects/$APP_NAME/config/combined-truststore.jks -Djavax.net.ssl.trustStorePassword=4s9aHujx9KvPpKKCSe7 -Dspring.cloud.config.uri=https://$ENV-service-gateway.service.consul:8443/$CONFIG_PATH -jar /data/projects/$APP_NAME/$APP_NAME-$app_version.#PACKAGE_EXTENSION#