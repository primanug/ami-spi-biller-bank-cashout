#!/bin/bash
#####################################################################################################
#  Pipeline Generator for Ascend's application														#
#  Create by devops team																			#
#  Ascend Group Co., Ltd.																			#
#####################################################################################################

# Add Docker Host Info
export LANG="en_US.UTF-8"
APP_NAME=$(cat /image_info/app_name)
APP_VERSION=$(cat /image_info/app_version)
#COUNTRY_CODE=$(cat /image_info/country_code)
RUNWAY_NAME=$(echo "$RUNWAY_NAME" | awk '{print tolower($0)}')

# chmod +x /usr/script/${RUNWAY_NAME}-startup.sh
cd /usr/script
exec ./${RUNWAY_NAME}-startup.sh