// common.groovy

def sendElasticsearchTrackingVersion(GLOBAL_VARS, namespace_cicd_common, namespace_env, envName, appVersion, GIT_HASH_OPENSHIFT_CONFIGURATION, trackingVersionList, resultPipeline, startTimePipeline, endTimePipeline) {

  try {

    acnSendElasticsearchTrackingVersion {
      namespace = namespace_cicd_common
      envOpenshift = namespace_env
      dataTrackingVersionList = trackingVersionList
      globalVariablesList = GLOBAL_VARS
      envName = envName
      appVersion = appVersion
      gitHashOpenshiftConfiguration = GIT_HASH_OPENSHIFT_CONFIGURATION.length() > 0 ? GIT_HASH_OPENSHIFT_CONFIGURATION : "N/A"
      gitHashEcsConfiguration = GLOBAL_VARS['RUNWAY_NAME'] == "ECS" && git_hash_configuration.length() > 0 ? git_hash_configuration : "N/A"
      gitHashTesseractConfiguration = GLOBAL_VARS['RUNWAY_NAME'] == "TESSERACT" && git_hash_configuration.length() > 0 ? git_hash_configuration : "N/A"
      resultPipeline = resultPipeline
      startTime = startTimePipeline
      endTime = endTimePipeline
    }

  } catch(e) {

    println "Error : Send elasticseach tracking version was failed"

  }

}

def syncBuildNumber(namespace_cicd, jobName, increaseConvertBuildNumber) {
    
  try {

    other_jenkin_url = sh script: "oc get configmap param-sync-buildnumber -n ${namespace_cicd} -o json | jq-linux64 .data.other_jenkin_url", returnStdout: true
    other_jenkin_url = other_jenkin_url.trim()
    
    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'other_jenkin_credential', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
      userOtherJenkin = env.USERNAME
      passOtherJenkin = env.PASSWORD
    }
    
    sh "curl -k -d \'script=Jenkins.instance.getItemByFullName(\"${jobName}\").updateNextBuildNumber(${increaseConvertBuildNumber})\' ${other_jenkin_url}/scriptText --user ${userOtherJenkin}:${passOtherJenkin}"
    println "Sync build number of ${jobName} to build ${increaseConvertBuildNumber}"

  } catch(e) {
      println "Error : Sync build number was failed"
  }

}

def buildMountBank(directory_workspace, GLOBAL_VARS, APP_VERSION, NAMESPACE_CICD, APPLICATION_MOUNTEBANK_EXISTING, listFileCommitBoolean, countryCode, openshiftVersion, BUILD_ON_BRANCH, namespace_dev) {
  
  if ( GLOBAL_VARS['DEPLOY_MOUNTBANK'].toInteger() == 1 ) {

    if( APPLICATION_MOUNTEBANK_EXISTING == 'application-MB-Not-Existing' || listFileCommitBoolean.contains(true) ) {

      echo "${countryCode}"

      imageApplicationMountebank = acnImageBuild {
        global_vars = GLOBAL_VARS
        appScope = GLOBAL_VARS['APP_SCOPE']
        appLang = GLOBAL_VARS['APP_LANG']
        countryCode = countryCode //value not pass to lib
        country_code = countryCode
        appName = GLOBAL_VARS['APP_NAME']
        packageExtension = GLOBAL_VARS['PACKAGE_EXTENSION']
        middlewareName = GLOBAL_VARS['MIDDLEWARE_NAME']
        directory = directory_workspace
        openshiftVersionFolder = openshiftVersion
        appVersion = APP_VERSION
        imageType = "mountebank"
        namespace = NAMESPACE_CICD
        envNameImage = "dev"
        branch = BUILD_ON_BRANCH
        // branch = "master"
      }
      
    } else {

      println "http://${GLOBAL_VARS['APP_NAME']}-mountebank.${namespace_dev}.svc:2525 already existing and no change artifact"

    } // End condition for take action to build images mountebank

  } else if ( GLOBAL_VARS['DEPLOY_MOUNTBANK'].toInteger() == 0 ) {
    
    // disable mountebank
    println "SKIP : Build mountebank image due to deploy mountebank state = ${GLOBAL_VARS['DEPLOY_MOUNTBANK']} (disable mountebank)"

  }

}

def updateConfig(directory_workspace, GLOBAL_VARS, countryCode, APP_VERSION, trackingVersionList) {

  try {

    dir("${directory_workspace}/update-config") {

      GIT_HASH_OPENSHIFT_CONFIGURATION = acnCompressConfiguration {
        global_vars = GLOBAL_VARS
        country_code = countryCode
        version = APP_VERSION
        directory = directory_workspace
        tracking_action_list = trackingVersionList
      }

    }
    
  } catch(e) {

    println 'Error : Update configuration was failed!'
    println e

    throw e
    
  }

}

// def unitTest(RERUN_CONDITION_ACTION,flagSkipDev,GLOBAL_VARS,APP_VERSION) {
// 	stage('Unit test & Source Analysis & Build Jar Package') {
//         stageCurrentDEV = "DEV_Unit-Test-And-Source-Analysis-and-Build-Jar-Package"
//         if ( RERUN_CONDITION_ACTION == "ignore" ) {
//           try{          
//             flagSkipDev.add("false")
//             acnMavenBuildandCodeAnalysis{
//               global_vars = GLOBAL_VARS
//               VERSION = APP_VERSION
//               } 
//           } catch(e) {
//             println 'Error : Something failed!'
//             throw e
//           }
//         } else {
//           echo "SKIP"
//         } // End RERUN_CONDITION_ACTION stage build jar
//       } // End stage Source analysis
// }

def sonarQube(GLOBAL_VARS) {

  try {

    initial_delay_verify_sonarqube = GLOBAL_VARS['SONARQUBE_VERIFY_TIMEOUT'].toInteger()
    
    sleep initial_delay_verify_sonarqube

    timeout(time: 1, unit: 'MINUTES') {

      script {
        def qg = waitForQualityGate()
        if (qg.status != 'OK') {
          error "Pipeline aborted due to quality gate failure: ${qg.status}"
        }
      }

    } // End scope timeout quality gate

  } catch(e) {
    
    println "Error : SonarQube quality check was failed!"
    println e

    throw e

  }

}

def devBuildImages(directory_workspace, GLOBAL_VARS, countryCode, openshiftVersion, APP_VERSION, namespace_cicd, namespace_dev, APPLICATION_MOUNTEBANK_EXISTING, listFileCommitBoolean, trackingVersionList) {

  try {

    images = acnParallelBuildImages {
      global_vars = GLOBAL_VARS
      country_code = countryCode
      directory = directory_workspace
      openshift_version = openshiftVersion
      app_version = APP_VERSION
      namespace = namespace_cicd
      namespaceDev = namespace_dev
      applicationMountebankExisting = APPLICATION_MOUNTEBANK_EXISTING
      listCommitBoolean = listFileCommitBoolean
      tracking_action_list = trackingVersionList
    }

    return images

  } catch(e) {

    println 'Error : Build images was failed!'
    println e

    throw e

  }

}

def devScanImages(directory_workspace, GLOBAL_VARS, images, countryCode, openshiftVersion, APP_VERSION) {

  try {

    echo "Scaning image : ${images[0]}"

    // Scan Application Image
    statusScanImage = acnImageAnchoreScan {
      global_vars = GLOBAL_VARS
      imageAppName = images[0]
      country_code = countryCode
      directory = directory_workspace
      openshift_version = openshiftVersion
      app_version = APP_VERSION
      jobFailIfEvaluateFailResult = false //true or false 
      jobFailIfPluginError = false //true or false
      numEngineRetries = '300'
    }

  } catch(e) {

    println 'Error : Image vulnerability scan was failed!'
    println e

    throw e

  }

}

def devArtifactDist(directory_workspace, GLOBAL_VARS, countryCode, APP_VERSION, GIT_HASH, images, trackingVersionList, GIT_HASH_OPENSHIFT_CONFIGURATION, openshiftVersion) {

  try {

    git_hash_configuration = acnCompressArtifacts {
      global_vars = GLOBAL_VARS
      country_code = countryCode
      app_version = APP_VERSION
      git_hash_application = GIT_HASH
      directory = directory_workspace
      app_image = images[0]
      tracking_action_list = trackingVersionList
      git_hash_openshift_configuration = GIT_HASH_OPENSHIFT_CONFIGURATION
      openshift_version = openshiftVersion
    }

    echo "git_hash_configuration ${git_hash_configuration}"

  } catch(e) {

    println 'Error : Artifact distributed was failed!'
    println e

    throw e

  }
  
}

def devSyncConfig(directory_workspace, GLOBAL_VARS, APP_VERSION, countryCode, LIST_ENV) {

  try {

    dir("${directory_workspace}/s3-pull-config") {

      acnSyncConfiguration_Status = acnSyncConfigurationServer{
        global_vars = GLOBAL_VARS
        list_env = LIST_ENV
        version = APP_VERSION
        country_code = countryCode
        config_path = "/app-configs"
        directory = directory_workspace
      }

    }

  } catch(e) {

    println 'Error : Sync configuration server was failed!'
    println e

    throw e

  }

}

def devDeployResToDev(directory_workspace, GLOBAL_VARS, countryCode, openshiftVersion, APP_VERSION, namespace_cicd, LIST_ENV, namespace_dev, APPLICATION_MOUNTEBANK_EXISTING, listFileCommitBoolean, trackingVersionList) {

  try {

    acnParallelDeploy {
      global_var = GLOBAL_VARS
      openshift_version = openshiftVersion
      app_version = APP_VERSION
      namespace = namespace_cicd
      list_env = LIST_ENV
      namespaceDev = namespace_dev
      applicationMountebankExisting = APPLICATION_MOUNTEBANK_EXISTING
      listCommitBoolean = listFileCommitBoolean
      buildDetailList = trackingVersionList
      directory = directory_workspace
      country_code = countryCode
      config_path = "/app-configs"
    }

  } catch(e) {

    println 'Error : Deploy resource to DEV Environment was failed!'
    println e

    throw e

  }

}

def devVerifyVersion(APP_URL_TYPE_SERVICE, GLOBAL_VARS, namespace_dev, APP_VERSION) {
    
  try {

    APP_URL_TYPE_SERVICE = new URL("${GLOBAL_VARS['APP_PROTOCOL']}://${GLOBAL_VARS['APP_NAME']}.${namespace_dev}.svc:${GLOBAL_VARS['APP_PORT']}${GLOBAL_VARS['PATH_INFO']}")

    acnVerifyVersion {
      global_vars = GLOBAL_VARS
      app_url_openshift_format = APP_URL_TYPE_SERVICE
      version = APP_VERSION
    }

  } catch(e) {
    
    println 'Error : Verify application version was failed!'
    println e

    throw e

  }

}

def devRunIntegrationTest(directory_workspace, GLOBAL_VARS, RERUN_CONDITION_ACTION, APP_VERSION, APP_URL_TYPE_SERVICE, trackingVersionList) {
    
  try {

    acnRunIntegrationTest {
      conditionForGetVersion = "dev"
      environmentForWorkspace = "dev"
      global_vars = GLOBAL_VARS
      rerun_condition_action = RERUN_CONDITION_ACTION
      app_version = APP_VERSION
      app_url_type_service = APP_URL_TYPE_SERVICE
      jobTMTId = GLOBAL_VARS['TMT_JOB_ID_DEV']
      authorizationTMTId = GLOBAL_VARS['TMT_AUTHORIZATION']
      test_tools = GLOBAL_VARS['TEST_TOOLS']
      directory = directory_workspace
      tracking_action_list = trackingVersionList
    }

  } catch(e) {

    println 'Error : Run integration test was failed!'
    println e

    throw e

  }

}

def qaDeployResToQa(directory_workspace, GLOBAL_VARS, countryCode, openshiftVersion, APP_VERSION, namespace_qa, trackingVersionList, gitOpsRepoName) {

  try {

    return acnGetDeploymentResources {
      global_vars = GLOBAL_VARS
      appName = GLOBAL_VARS['APP_NAME']
      versionOpenshift = openshiftVersion
      appVersion = APP_VERSION // APP_VERSION From DEV
      envName = "qa"
      replicaNum = GLOBAL_VARS['DEFAULT_NUM_REPLICA_QA']
      routeHostname = GLOBAL_VARS['ROUTE_HOSTNAME_QA']
      routerSharding = GLOBAL_VARS['ROUTE_SHARDING']
      namespace_env = namespace_qa
      gitHashApplication = trackingVersionList[2]
      gitSourceBranch = trackingVersionList[5]
      forceDeployList = trackingVersionList
      directoryWorkspace = directory_workspace
      country_code = countryCode
      deploymentType = "no_action_type_in_qa"
      dbDeploymentEnable = "no_action_type_in_qa"
      releaseTransId = "no_trans_id_type_in_qa"
      config_path = "/app-configs"
      deploy_mountebank_enable = "no_action_type_in_qa"
      gitOpsRepoName = gitOpsRepoName
    }

  } catch(e) {

    println 'Error : Deployment resources for performance environment was failed!'
    println e

    throw e

  }

}

def qaRunSmokeTest(directory_workspace, GLOBAL_VARS, RERUN_CONDITION_ACTION, APP_VERSION, APP_URL_TYPE_SERVICE, trackingVersionList) { 

  try {

    acnRunSmokeTest {
      conditionForGetVersion = "qa"
      environmentForWorkspace = "qa"
      global_vars = GLOBAL_VARS
      rerun_condition_action = RERUN_CONDITION_ACTION
      app_version = APP_VERSION // APP_VERSION From DEV
      app_url_type_service = APP_URL_TYPE_SERVICE
      jobTMTId = GLOBAL_VARS['TMT_JOB_ID_QA']
      authorizationTMTId = GLOBAL_VARS['TMT_AUTHORIZATION']
      test_tools = GLOBAL_VARS['TEST_TOOLS']
      directory = directory_workspace
      tracking_action_list = trackingVersionList
    }

  } catch(e) {

    println 'Error : Run smoke test was failed!'
    println e

    throw e

  }

}

def qaRunIntegrationTest(directory_workspace, GLOBAL_VARS, RERUN_CONDITION_ACTION, APP_VERSION, APP_URL_TYPE_SERVICE, trackingVersionList) {

  try {

    acnRunIntegrationTest {
      conditionForGetVersion = "qa"
      environmentForWorkspace = "qa"
      global_vars = GLOBAL_VARS
      rerun_condition_action = RERUN_CONDITION_ACTION
      app_version = APP_VERSION // APP_VERSION From DEV
      app_url_type_service = APP_URL_TYPE_SERVICE
      jobTMTId = GLOBAL_VARS['TMT_JOB_ID_QA']
      authorizationTMTId = GLOBAL_VARS['TMT_AUTHORIZATION']
      test_tools = GLOBAL_VARS['TEST_TOOLS']
      directory = directory_workspace
      tracking_action_list = trackingVersionList
    }

  } catch(e) {

    println 'Error : Run integration test was failed!'
    println e

    throw e

  }

}

def perfDeployResToPerf(directory_workspace, GLOBAL_VARS, countryCode, openshiftVersion, APP_VERSION, namespace_performance, trackingVersionList, gitOpsRepoName) {

  try {

    return acnGetDeploymentResources {
      global_vars = GLOBAL_VARS
      appName = GLOBAL_VARS['APP_NAME']
      versionOpenshift = openshiftVersion
      appVersion = APP_VERSION // APP_VERSION FROM QA
      envName = "performance"
      replicaNum = GLOBAL_VARS['DEFAULT_NUM_REPLICA_PERFORMANCE']
      routeHostname = GLOBAL_VARS['ROUTE_HOSTNAME_PERFORMANCE']
      routerSharding = GLOBAL_VARS['ROUTE_SHARDING']
      namespace_env = namespace_performance
      gitHashApplication = trackingVersionList[2]
      gitSourceBranch = trackingVersionList[5]
      forceDeployList = trackingVersionList
      directoryWorkspace = directory_workspace
      country_code = countryCode
      deploymentType = "no_action_type_in_performance"
      dbDeploymentEnable = "no_action_type_in_performance"
      releaseTransId = "no_trans_id_type_in_performance"
      config_path = "/app-configs"
      deploy_mountebank_enable = "no_action_type_in_performance"
      gitOpsRepoName = gitOpsRepoName
    }

  } catch(e) {

    println 'Error : Deployment resources for performance environment was failed!'
    println e

    throw e

  }

}

def perfRunSystemIntegrationTest(directory_workspace, GLOBAL_VARS, APP_VERSION, RERUN_CONDITION_ACTION, APP_URL_TYPE_SERVICE, trackingVersionList) {

  try {

    acnSystemIntegrationTest {
      global_vars = GLOBAL_VARS
      app_version = APP_VERSION // APP_VERSION FROM QA
      rerun_condition_action = RERUN_CONDITION_ACTION
      app_url_type_service = APP_URL_TYPE_SERVICE
      conditionForGetVersion = "performance"
      directory = directory_workspace
      tracking_action_list = trackingVersionList
    }

  } catch(e) {

    println 'Error : Run system integration test was failed!'
    println e

    throw e

  }

}

def perfRunPerformanceTest(directory_workspace, GLOBAL_VARS, trackingVersionList) {

  try {

    acnPerformanceTest {
      global_vars = GLOBAL_VARS
      directory = directory_workspace
      tracking_action_list = trackingVersionList
    }

  } catch(e) {

    println 'Error : Run Performance Test was failed!'
    println e

    throw e
    
  }

}

return this
